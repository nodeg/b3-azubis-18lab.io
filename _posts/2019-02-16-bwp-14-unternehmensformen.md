---
layout: post
title:  "14. Unternehmensformen"
date:   2019-02-15 06:50:00 +0100
category: 1. Lehrjahr
tags: [Betriebswirtschaftliche Prozesse]
---

# Unternehmensformen

## Einzelunternehmen

- **Gründung**:
  - durch den Einzelunternehmer (alleiniger Gesellschafter des Unternehmens)
  - formlos - Gewerbeanmeldung (keine besonderen gesetzlichen Formvorschriften)
  - evtl. HR-Eintragung: Firma, Sitz, Gegenstand
- **Firma**:
  - Ohne HR-Eintrag: Vor- und Nachnahme des Geschäftsführers & Ergänzung um Branchen- oder Fantasienamens
  - Mit HR-Eintrag: Personen-, Sach- oder Fantasiename & Rechtsformzusatz (e.K., e.Kfr. oder e.Kfm.)
- **Kapitalaufbringung**: Kein gesetzliches Mindestkapital $\rightarrow$ Einzelunternehmer bringt Kapital alleine auf.
- **Geschäftsführung** & Vertretung: Durch den Einzelunternehmer
- **Haftung**: Unbeschränkt (mit Privat- und Gesellschaftsvermögen) & unmittelbar (direkt verklagbar)
- **Ergebnisverteilung**: alleiniger Gewinnanspruch & alleinige Verlustübernahme
- **Publizität**: keine Publizitätspflicht (keine Veröffentlichung des Geschäftsergebnisses)
- **Besteuerung**:
  - Einkommenssteuer
  - Gewerbesteuer

**Vorteile**:
- Geringer Aufwand zur Gründung und Führung: Einfacher Weg in die Selbstständigkeit
- Alleinige Gewinnbeteiligung

**Nachteile**:
- Haftung mit Privatvermögen

## Gesellschaftsunternehmen

### Personengesellschaft

#### OHG

- OHG = Offene Handels Gesellschaft
- Zweck: Betreiben eines Handelsgewerbes mit mehreren Personen in einer Firma.
- Stellt eine erweiterte Einzelunternehmung dar mit dem Motto "Gleiche Rechte - Gleiche Pflichten"
- i. d. R. für kleinere und mittlere Betriebe

- **Gründung**: Gesellschaftsvertrag zwischen zwei oder mehr Personen & Eintragung ins HRA mit Namen, Geb. Datum und Wohnort der Gesellschafter, sowie Firmensitz und Beginn der Gesellschaft
- **Geschäftsführung**: Jeder Gesellschafter vertritt das Unternehmen nach außen und ist somit berechtigt und verpflichtet die Geschäfte zu führen, jedoch können Ausnahmen mit dem Gesellschaftsvertrag geregelt werden. Für außergewöhnliche Geschäfte ist der Beschluss aller Gesellschafter erforderlich.
- **Haftung**: Für Verbindlichkeiten der Firma haftet jeder Gesellschafter unbeschränkt als Gesamtschuldner mit dem Privat- und Betriebsvermögen.
- **Ergebnisverteilung**: Jeder Gesellschafter erhält, ohne andere Regelung im Gesellschaftsvertrag, 4 % seiner Kapitaleinlage. Gewinn der übrig bleibt wird in gleichen Teilen an die Gesellschafter ausgeschüttet. Verluste werden gleichermaßen auf die Gesellschafter aufgeteilt. Ein Gesellschafter hat die Möglichkeit bis zu 4 % seines Kapitalanteils für private Zwecke zu entnehmen.
- **Finanzierung**: Die erhöhte Kreditwürdigkeit der OHG kommt zustande durch die Haftung von mindestens zwei Gesellschaftern mit ihrem Gesamtvermögen. Die Kapitalbasis wird erhöht durch nicht an die Gesellschafter ausgeschüttete Gewinne, die Aufnahme eben derer und Gesellschafter welche ihre Kapitaleinlagen erhöhen.
- **Besteuerung**: Die OHG ist kein selbstständiges Steuersubjekt, somit besteht eine Einkommenssteuerpflicht der Gesellschafter zum Zeitpunkt der Gewinnentstehung im Betrieb. Die Gehälter der geschäftsführenden Gesellschafter sind nicht als Betriebsausgaben abzugsfähig.
- **Auflösung**: Sollten keine anderen vertraglichen Regelungen getroffen werden gelten folgende Regelungen. Die Auflösung der OHG kann durch den Ablauf des Gesellschaftsvertrags, Beschluss der Gesellschafter, Eröffnung des Insolvenzverfahrens über des Gesellschaftsvermögens oder eine gerichtliche Entscheidung geschehen. Der Tod oder die Kündigung eines Gesellschafters hat keine Bewandtnis für die Existenz einer OHG. 

#### KG

- KG = Kommanditgesellschaft
- Zweck: Betreiben eines Handelsgewerbes mit mehreren Personen in einer Firma.
- Komplementär: Vollhaftende natürliche Person (Privat- und Firmenvermögen)
- Kommanditist: Teilhaftender Gesellschafter, kann natürlich als auch juristisch sein. Ist ein Komplementär eine GmbH, so firmiert die KG als _GmbH & Co KG_.

- **Gründung**: Eintragung ins HRA und Gesellschaftsvertrag, es ist im HR die Haftsumme (Einlage) der Kommanditisten eingetragen.
- **Geschäftsführung**: Die Komplementäre führen die Geschäfte der KG, Kommanditisten haben lediglich ein Kontrollrecht.
- **Haftung**: Ein Komplementär haftet unbeschränkt mit Privat- und Firmenvermögen. Ein Kommanditist lediglich mit der Einlage.
- **Ergebnisverteilung**: Wie bei der OHG erhält jeder Gesellschafter bis zu 4 % seiner Kapitaleinlage. Alles darüber hinausgehende muss im Gesellschaftsvertrag geregelt sein. I. d. R. ist es üblich, dass der Komplementär wegen seines höheren Risikos einen größeren Anteil erhält als ein Kommanditist. Auch Verluste werden "angemessen" aufgeteilt.
- **Finanzierung**: Die KG ähnelt einer Kapitalgesellschaft, da sie Kommanditisten aufnehmen kann, welche nicht mit der Geschäftsführung betraut werden. Kommanditisten haften nur beschränkt und sind somit keinem größeren Risiko ausgesetzt. Die erhöhte Transparenz und einfache Möglichkeit zur Kapitalerhöhung werden zumeist auch von Kreditgebern honoriert.
- **Besteuerung**: Der Gewinn wird einheitlich von der Gesellschaft ermittelt, dann nach einem bestimmten Schlüssel auf die Gesellschafter verteilt und von diesen individuell versteuert. Dies rührt daher, dass die KG eine Personengesellschaft ist.
- **Auflösung**: Kündigung oder Ablauf des Gesellschaftsvertrages, Beschluss der Gesellschafter, Konkurs über das Vermögen der Gesellschaft oder eines Komplementärs und durch den Tod eines Komplementärs, sofern der Gesellschaftsvertrag nichts anderes vorsieht.

### Kapitalgesellschaft

#### AG
TODO


#### GmbH
TODO


### Genossenschaft
TODO
