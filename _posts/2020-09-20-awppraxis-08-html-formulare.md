---
layout: post
title: "08. HTML Formulare"
date: 2020-03-20 10:20:00 +0200
category: 2. Lehrjahr
tags: [Anwendungsprogrammierung, Praxis]
---

## Der HTML Teil von Formularen

Zum Verfassungszeitpunkt zählt die Liste von Eingabefeldtypen 21. Diese Liste wird jedoch von Zeit zu Zeit mit neuen
RFCs erweitert. Die aktuelle Liste ist zu finden unter [Link][2].

Ein Formular ist Grundsätzlich folgendermaßen aufgebaut:

```html
<form method="post" action="ziel-url">
    <!-- hier alle Elemente des Formulars-->
</form>
```

Die Parameter `method` und `action` sind optional, aber zumeist sinnvoll. Der Parameter `method` legt fest mit welcher
HTTP-Methode der Browser das Ergebnis des Formulars an den Server schickt, die gängigsten Formate sind `GET` und `POST`.
Der Parameter `action` legt fest wo das Ziel für das Ergebnis des Formulars ist. Der Standard für `method` ist `GET` und
der Standard für `action` ist die aktuelle Seite. Was man in ein Formular alles für Inhalte setzen kann, ist unter
obigem Link zu finden oder einige Beispiele unter diesem Paragraphen. 

### Texteingabefeld

```html
<input type="text" name="txtName" />
```

### Checkbox/Kontrollkästchen

```html
<input type="checkbox" id="chkRueckruf" name="chkRueckruf" value="Yes" checked />
<label for="chkRueckruf">Ja</label> 
```

### Auswahlliste

```html
<select name="optRueckrufZeit" >
    <option value="1"> 08:00 - 10:00 </option>
    <option value="2" selected> 10:00 - 12:00 </option>
    <option value="3"> 12:00 - 14:00 </option>
</select>

```

### Radiobutton/Optionsfeld

```html
<input type="radio" id="raKatalogYes" name="raKatalog" value="Yes" />
<label for="raKatalogYes">Ja</label>
<input type="radio" id="raKatalogNo" name="raKatalog" value="No" checked />
<label for="raKatalogNo">Nein</label>
```

### Befehlsschaltfläche

```html
<input type="reset" value="Zurücksetzen" />
<input type="submit" name="btnDatenSenden" value="Abschicken" />
```

## Der PHP-Teil von Formularen & PHP-Arrays

Um Formulare die vom Browser an unsere PHP Applikation gesendet werden auszuwerten zu können, müssen wir einen kurzen
Exkurs machen zu PHP-Arrays. Die offizielle Anleitung ist [hier][1] zu finden.

Ein Array ist eine Liste von Werten des gleichen Typs, die im Speicher hintereinander liegen. Die Liste ist eine einzige
Variable und um auf die einzelnen Objekte in der Liste zuzugreifen benötigt man den sogenannten Index. Der Index ist im
Fall von PHP entweder eine Ganzzahl (bzw. ein Integer) oder eine Zeichenkette. Untenstehend sind einige Beispiele mit
jeweils in der Zeile darüber einem Kommentar was die darauffolgende Zeile macht.

```php
# Erstellung eines leeren Arrays in PHP
$myarray1 = array();

# Erstellung eines Arrays mit Inhalten in PHP
$myarray2 = array(
    0 => "Erstes Element",
    1 => "Zweites Element",
);

# In PHP müssen Arrays nicht mit Null anfangen wie bspw. in C++
$myarray3 = array(
    99 => "Erstes Element",
    100 => "Zweites Element",
);

# Der Arrayindex wird immer in einen String Konvertiert. Deshalb gilt folgendes:
$myarray4 = array(1 => "Aufpassen!", true => "Falscher Wert ausgegeben!");
var_dump($myarray4); # Ergibt: "array(1) { 1=> "Falscher Wert ausgegeben!" }"

# Greife lesend auf einen Wert des Arrays zu
var_dump($myarray3[99]); # Ergibt: string(14) "Erstes Element"

# Ein Array löschen
unset($myarray1);

# Dem Array ein neues Element hinzufügen
$myarray2[3] = "Drittes Element";
$myarray2[] = "Viertel Element"; # Das hier ist sogenannter syntactic sugar, versucht zu vermeiden das zu benutzen.
``` 

So nun zum eigentlichen Thema dieses Abschnitts: Wie werte ich ein Formular in PHP aus?

Hierzu gibt es zwei Dinge: `$_GET` und `$_POST`. Diese beiden Spezialarrays werden automatisch mit den POST und GET
Variablen die der Request enthält befüllt. Diese enthalten Nutzerdaten und sind somit als "feindlich", "verseucht" bzw.
unsicher zu behandeln. Dies meint das sie Schadcode enthalten können und zuallererst validiert werden müssen bevor sie
weiter im Programm verwendet werden.

Um nicht auf Schlüssel zuzugreifen, die eventuell nicht vorhanden sind in diesen automatisch generierten Arrays, stellt
PHP uns die Methode `(isset(<ARRAY>)` zur Verfügung. Verknüpfen wir diese Funktion mit einer IF-Verzweigung, so können
wir unser Skript/Programm stabiler konstruieren. Untenstehend ein Rohgerüst als Beispiel:

```php
if (isset($_POST["SCHLUESSEL"])) {
    # Schlüssel vorhanden
} else {
    # Schlüssel nicht vorhanden
}
```

> Anmerkung: Die Schlüssel in `$_POST` und `$_GET` sind Case-Sensitive. "Schluessel" ist nicht gleich "schluessel".

## Debugging und Manipulierte Requests im Browser

Um Webapplikationen einfacher debuggen zu können stellen alle Browserhersteller die sogenannten "Developer Tools" oder
auf Deutsch "Entwickler Tools" zur Verfügung. Sie sind zu öffnen indem auf der Tastatur die Taste F12 gedrückt wird.

---

[1]: https://www.php.net/manual/de/language.types.array.php
[2]: https://www.w3schools.com/html/html_form_input_types.asp