---
layout: post
title: "06. HTML"
date: 2020-03-20 10:00:00 +0200
category: 2. Lehrjahr
tags: [Anwendungsprogrammierung, Praxis]
---

> HTML = Hypertext-Markup Language

HTML ist eine Auszeichnungssprache. Sie dient der einfachen Strukturierung und Darstellung von Texten, Bildern, Tabellen
und anderen Inhalten im World Wide Web und kann mit Hilfe von Webbrowsern dargestellt werden. Grundsätzlich besteht jede
moderne Webseite aus den folgenden drei Komponenten:

- Inhalt → HTML
- Design → CSS
- Interaktion/Logik → JavaScript (clientseitig) und/oder PHP (serverseitig)

Eine HTML Seite besteht aus HTML-Tags. Je nach HTML-Tag kann dieser Attribute und Inhalte besitzen.

````
    <td    valign    =     "top"     >  Hier steht der Inhalt   </td>
Start tag Attribute     Attributwert           Inhalt           Endtag
````

Die korrekte Verwendung nachfolgender HTML-Tags wird vorausgesetzt:

- HTML Grundstruktur `<head> <title> <body>`
- Überschriften `<h1> <h2> ...`
- Zeilenumbruch `<br>`
- Horizontale Linie `<hr>`
- Abschnitte `<p>`
- Tabellen `<table> <tr> <thead> <th> <tbody> <td>`
- Bilder `<img ...>`
- Hyperlinks `<a href="...">`
- Listen `<ul> <ol> <li>`
- HTML Kommentare `<!--Mein Kommentar-->`

Benötigte Attribute zur Formatierungen:

- Ausrichtungen: `align, valign`
- Hintergrundfarbe: `bgcolor`
- Breite: `width`
- Höhe: `height`
- Spaltenverbund: `colspan`
- Zeilenverbund: `rowspan`
- Tabellenrahmen: `border`

Weitere Quellen für das Selbststudium:
- [Selfhtml](wiki.selfhtml.org/wiki/HTML)
- [Mozilla-Developper](developer.mozilla.org/de/docs/Learn/HTML/Einf%C3%BChrung_in_HTML)
- [HTML-Einfach](https://html-einfach.de/)
- [W3 Schools](https://www.w3schools.com/html/default.asp)
- [Youtube](https://www.youtube.com/watch?v=h5nEfuxdVS0)
- oder Videos von [Simpleclub](https://simpleclub.de)

Schon fertig?
- Testen Sie Ihr HTML Wissen: [link](https://www.w3schools.com/quiztest/quiztest.asp?qtest=HTML)
- Oder besuchen Sie die erste Webseite des [WWW](http://info.cern.ch/)
