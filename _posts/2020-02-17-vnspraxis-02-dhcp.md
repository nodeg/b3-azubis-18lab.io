---
layout: post
title:  "02. DHCP"
date:   2020-02-04 07:45:00 +0100
category: 2. Lehrjahr
tags: [Vernetzte Systeme, Praxis]
---

Wenn in einem Netzwerk den Clients beim Systemstart automatisch eine IP-Adresse zugewiesen werden soll benötigt es
hierfür einen Dienst. Diese Aufgabe übernimmt das Dynamic Host Configuration Protocol, das einem Endgerät nach einem
DHCP-Request eine IP-Adresse zuweist. Die Adresszuweisung kann dabei statisch oder dynamisch erfolgen:

- **Statische Adresszuweisung**: Der DHCP Server weist dem Host anhand der MAC-Adresse eine feste IP zu. 
- **Dynamische Adresszuweisung**: Aus einem Adresspool(range) weist der DHCP-Server dem Client eine variable bzw.
  temporäre IP zu.

![VNS-Praxis - DHCP Adresszuweisung](/assets/img/VNS-Praxis/02-dhcp-ablauf-adresszuweisung.png)

Quelle: [https://de.wikipedia.org/wiki/Datei:16a_DHCP.png](https://de.wikipedia.org/wiki/Datei:16a_DHCP.png)

## Einsatz von DHCP unter Windows

Bevor DHCP eingesetzt werden kann, muss der Dienst Microsoft DHCP-Server zuerst auf dem Windows-Server installiert
werden. Zu den Konfigurationen unter DHCP zählen die Angabe des IP-Adressbereichs und die dazugehörige Subnetzmaske. Auf
diese Weise kann genau festgelegt werden, welche IP-Adressen verfügbar sind und vergeben werden dürfen. IP-Adressen
sollten dann ausgeschlossen werden, wenn sie statisch einem bestimmten Host zugeordnet sind oder wenn sie bereits zum
Bereich eines anderen DHCP-Servers gehören.

**ACHTUNG:**  
Aktivieren Sie niemals DHCP für ein System, das Ressourcen zur Verfügung stellt! Das schließt nicht nur
Domänen-Controller ein, sondern auch alle anderen Server (einschließlich Arbeitsstationen, die Verzeichnisse oder
Drucker freigeben), Anbieter von Netzwerkdiensten (DNS, WINS, usw.), Druck-Server und so weiter.
