---
layout: post
title:  "03. Rechte und Pflichten von AN und AG"
date:   2019-02-11 06:10:00 +0100
category: 1. Lehrjahr
tags: [Sozialkunde]
---

# Rechte und Pflichten von Arbeitnehmer (AN) und Arbeitgeber (AG)

| Pflichten der Arbeitnehmer                                   | Pflichten der Arbeitgeber                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| - Arbeitspflicht: Die im Arbeitsvertrag vereinbarte Leistung persönlich erbringen.<br>- Weisungsgebundenheit: AN muss Anweisungen des Vorgesetzten folgen<br>- Treuepflicht: AN darf Betriebsgeheimnisse und Kundendaten nicht an Dritte weitergeben $$\rightarrow$$         Ausnahmen: Ärzte, Priester<br>- Sorgfaltspflicht: AN muss Tätigkeiten nach bestem Wissen ausführen und Betriebsmittel pfleglich behandeln<br>- Erholungspflicht: Andere Tätigkeiten gegen Entgelt an Wochenenden oder im Urlaub sind nur mit         Zustimmung des AG zulässig | - Entgeltpflicht: AG muss vereinbarte Vergütung und Nebenleistungen pünktlich ausbezahlen<br>- Fürsorgepflicht: AG muss sozialen Arbeitsschutz (Arbeitszeit- und Urlaubsregelung, Unfallverhütungsvorschriften) einhalten<br>- Beschäftigungspflicht: AG muss AN entsprechend dem Arbeitsvertrag beschäftigen<br>- Zeugnispflicht: AG muss bei Beendigung des Arbeitsverhältnisses dem AN ein Arbeitszeugnis ausstellen. |

Wege ein Arbeitsverhältnis zu beenden:

1. Aufhebungsvertrag

   = Beide Seiten beenden das Arbeitsverhältnis einvernehmlich unter bestimmten Konditionen

2. Ordentliche Kündigung

3. Außerordentliche (fristlose) Kündigung

4. Ablauf eines befristeten Arbeitsverhältnisses
