---
layout: post
title:  "03. Projektplanung"
date:   2019-02-12 06:10:00 +0100
category: 1. Lehrjahr
tags: [PlusD]
--- 

Im Folgenden sollen einige Begriffe zur Projektplanung definiert werden:

## Definitionsphase

- Bildet verbindliche Grundlage des Projektes und enthält Definition des Projektziels (eindeutig und vollständig)
- Organisation des Projektes (Rahmenbedingungen)
- Kosten und Terminziele sowie die Organisation des Prozesses

## Ist-Zustand

Beschreibung der aktuellen Situation und der Prozesse (Wie läuft es aktuell ab?)  
$\rightarrow$ Dokumentenanalyse, Gespräche, Beobachtung, etc.

## Konzept

Ein Konzept ist eine möglichst genaue Beschreibung des geplanten Projektablaufes und somit Bestandteil des Pflichtenheftes. Je nach Detaillierungsgrad spricht man von Grob- oder Feinkonzept.

## Konzeptauswahl

Aus den Konzeptalternativen wird die beste Alternative ausgewählt. Dabei sind Aufwand und Nutzen zu betrachten.

## Lastenheft

Das Lastenheft enthält alle Anforderungen des Auftraggebers (Was und Wofür?)

- Ausgangssituation und Zielsetzung
- Produkteinsatz
- Produktübersicht
- Funktionale Anforderungen
- Nicht-funktionale Anforderungen
- Lieferumfang
- Abnahmekriterien 

## Pflichtenheft

Das Pflichtenheft beschreibt die Realisierung aller Anforderungen (Wie? Womit?).

- Zielbestimmung
- Produkteinsatz
- Produktkonfiguration
- Produkt-Funktion
- Produkt-Leistung
- GUI, Zugriffsrechte
- Qualitätsziele, Gesetze, Normen
- Testfälle, Testdaten

## Planung

Erstellung eines Projektstrukturplans (PSP) $\rightarrow$ Gliederung in Arbeitspakete. Arbeitspakete werden fein geplant und mit Zeit, Terminen und Ergebnissen festgelegt.  
Festlegung der Projektrollen und Spielregeln

## Realisierungsphase

Umsetzung des Pflichtenhefts

## Soll-Zustand

Gewünschter Zustand der Funktionen und Prozesse.

## Testphase

Umfangreiche Funktions- und Integrationstest der realisierten Lösung. Iterationen möglich.

## Wartungsphase

Ein Projekt endet nicht mit der Einführung, sondern auch die Wartung muss organisiert werden.
