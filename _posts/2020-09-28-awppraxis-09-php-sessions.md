---
layout: post
title: "09. PHP-Sessions"
date: 2020-03-29 10:00:00 +0200
category: 2. Lehrjahr
tags: [Anwendungsprogrammierung, Praxis]
---

## Was ist eine Session?

Moderne Applikationen können nicht abgebildet werden indem ein einziger Aufruf an einen Webserver getätigt wird. Sendet
ein Client jedoch eine Anfrage an einen Webserver, so sieht diese Anfrage (ohne zusätzliche Hilfsmittel) genauso aus
wie jede andere auch. Lediglich die IP-Adresse könnte zur Identifikation genutzt werden. Da jedoch (wie an einer
Berufsschule bspw.) mehrere Clients über eine IP-Adresse häufig Anfragen an einen Webserver schicken, kann dies nicht
als zuverlässiges Merkmal genutzt werden.

Um dieses Problem zu lösen, muss somit eine eindeutige Zuordnung von Client zu mehreren Anfragen, die zusammen gehören,
ermöglicht werden. Hierzu bedient man sich der Technik der Sessions. Diese ist nur möglich, wenn ein sogenanntes Cookie
gesetzt wird. Dieses Cookie hat als Inhalt ein Identifikationsmerkmal, welches auf dem Server für eine gewisse (kurze)
Zeit ein Gegenstück hat. Dieses Merkmal ist im allgemeinen geheim, da dies als Zwischenspeicher für wichtige
Informationen genutzt wird.

In PHP ist dies per default auf dem Client ein Cookie mit dem Namen `PHPSESSID`, welcher einen zufälligen Text aus
Buchstaben und Zahlen enthält, welcher Serverseitig zufällig generiert wurde, enthält. Auf dem Server liegt nun in einem
temporären Verzeichnis eine Datei, welche denselben Namen trägt, wie der Cookie an Inhalt hat. Somit ist eine Zuordnung
hergestellt, wenn der Browser mit jeder Anfrage den Cookie mit in seiner Anfrage überträgt.

Ein Client hat niemals das Wissen darüber welche Inhalte in der Session Serverseitig zwischen gespeichert werden. Er
sendet das Cookie lediglich als Identifikation mit.

> ACHTUNG: Eine Session ist in der modernen Welt nicht mehr als "sicheres" Identifikationsmerkmal anzusehen. Eine
> Session kann geklaut werden von einem Angreifer. Die Identifikation das die Anfrage von einem Client kommt, muss
> mindestens über einen oder mehrere weitere Wege gewährleistet werden. Welche dies sind, ist von Programm zu Programm
> unterschiedlich.

## `$_SESSION` in PHP

In PHP gibt es bestimmte superglobale (bzw. immer magisch verfügbare) Variablen, welche einen bestimmten Zweck erfüllen
und von PHP automatisch mit bestimmten Inhalten gefüllt werden. Die Variable `$_SESSION` enthält immer die Werte zu den
jeweiligen Schlüsseln der Session, welche von PHP gefunden wurde. Da eine PHP Anwendung nicht zwangsweise eine Session
besitzen muss, kann dieses Array auch leer sein bzw. nicht existieren.

Eine Session in PHP startet man mit der Funktion `session_start()` und die Inhalte müssen von der Anwendung befüllt
werden. Eine Session kann beendet werden mit `session_destroy()`.

Der lesende Zugriff auf diese superglobale Variable erfolgt wie bei einem normalen Array:
`$Groesse = $_SESSION[“txtSize“];`

Der schreibende Zugriff auf diese superglobale Variable erfolgt ebenfalls wie bei einem normalen Array:
`$_SESSION[‘txtSize‘] = $Groesse;`

Aufgrund der Besonderheiten von PHP kann in diesem Array natürlich mit einem belibigen Schlüssel oder Index natürlich
auch ein weiteres Feld gespeichert werden. Der folgende Code ist also natürlich vollkommen valide:
`echo $_SESSION['Noten'][1];` Lesbarer wäre allerdings untenstehender Codeblock:

```php
$meineNoten = $_SESSION [‘Noten‘];
echo $meineNoten[1];
```
