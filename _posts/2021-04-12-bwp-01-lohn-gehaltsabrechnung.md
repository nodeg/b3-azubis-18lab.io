---
layout: post
title: "01. Lohn- und Gehaltsabrechnung"
date: 2020-09-28 09:50:17 +0200
category: 3. Lehrjahr
tags: [ Betriebswirtschaftliche Prozesse]
---

## Steuerliche Abzüge

- Lohnsteuer (laut Lohnsteuertabelle und Steuerklasse)
- Kirchensteuer (Bayern: 8% der Umsatzsteuer, rest 9%)
- Solidaritätszuschlag (5,5% der Lohnsteuer)

==> Bemessungsgrundlage ist das Steuerpflichtige Bruttoeinkommen

## Sozialversicherungen

- Krankenversicherung: 14,6%
  - AG & AN je 7,3%
  - ggf. AN mehr, je nach Krankenkasse
  - + jeweils die Hälfte des Zusatzbeitrags
- Rentenversicherung: 18,6%
  - AG & AN je 9,3%
- Pflegeversicherung: 3,05%
  - AG & AN je 1,525%
  - kinderlose Menschen ab 23 Jahren: +0,25%
- Arbeitslosenversicherung: 2,4%
  - AG & AN je 1,2%

==> Bemessungsgrundlage ist das Sozialversicherungspflichtige Bruttoeinkommen

## Steuerklassen

- 1: Ledig und Alleinstehend
- 2: Alleinerziehend (Kindergeldempfänger)
- 3+5: eingetragene Lebenspartnerschaft bzw. verheiratet (3 verdient über 20% mehr als 5)
- 4+4: eingetragene Lebenspartnerschaft bzw. verheiratet (bei ca. gleichem Einkommen)
- 6: zusätzliche Jobs

## Lohnsteuertabelle

Ein Arbeitnehmer verdient bei der interRad GmbH monatlich 2.800€, ist in der Steuerklasse IV eingeordnet, hat 1 Kind
unter 18 Jahren (Zähler 1 für 1,0 Kinderfreibeträge auf der Steuerkarte), gehört einer christlichen
Religionsgemeinschaft an und hat seinen Wohnsitz im Bundesland Bremen.

Steuerlich liegt das Gehalt in der Stufe von 2798,99€ bis 2801,99€. Berechnungsgrundlage ist die Obergrenze der Stufe.
Sie ist in der ersten Spalte fett gedruckt (es zählt also der höhere Wert). Die Steuern werden in diesem Beispiel im
2. Bereich "I, II, III, IV mit der Zahl der Kinderfreibeträge ..." angelesen.

Da das Kindergeld direkt ausgezahlt wird, werden bei der Lohnsteuerberechnung keine Kinderfreibeträge berücksichtigt.
Die Hauptspalte LSt (1. Hauptspalte des 2. Bereiches) zeigt für die Steuerklasse IV den Betrag von 422,33€.

Die Hauptspalten 0,5 bis 3 zeigen den Solidaritätszuschlag und die Kirchensteuer. Die Überschriften der Hauptspalten
geben jeweils die Anzahl der Kinderfreibeträge an. Für die Steuerklasse IV ergibt sich bei dem Zähler 1
(1 Kinderfreibetrag) in der Spalte SolZ ein Solidaritätszuschlag von 18,42€ un in der Spalte 9% (Kirchensteuersatz für
fast alle Bundesländer) eine Kirchensteuer von 30,15€.

![BWP - Lohnsteuertabelle](/assets/img/BWP/01-tabelle.png)

## Rechenbeispiel

Für die Gehaltsabrechnung der von Ilona Evert (20 Jahre alt) liegen folgende Angaben vor: Bruttogehalt: 2.380,00 €,
unverheiratet, keine Kinder, römisch-katholisch, Lohnsteuerfreibetrag: 310,00 € pro Monat. Sonderzahlung wegen
Erreichung des Abteilungsziels: 200,00 €.

Es besteht ein Vertrag über vermögenswirksame Leistungen über 40,00 €, der Arbeitgeber beteiligt sich mit 26,00 €.

Ihre Krankenversicherung verlangt einen Zusatzbeitrag von 0,9 %.

| Punkt der Gehaltsabrechnung                              | Betrag in € | Betrag in € |
| -------------------------------------------------------- | ----------- | ----------- |
| Tarif- oder Grundgehalt/-lohn                            | 2380,00     |             |
| + Zuschläge                                              | 0,00        |             |
| + Sonderzahlungen                                        | 200,00      |             |
| + Arbeitgeberanteil zu VL                                | 26,00       |             |
| = sozialversicherungspflichtiges Bruttogehalt            |             | 2606,00     |
| - Steuerfreibetrag                                       | 310,00      |             |
| = steuerpflichtiges Bruttogehalt                         | 2296,00     |             |
| - Lohnsteuer (lt. Steuertabelle)                         | 239,66      |             |
| - Solidaritätszuschlag (lt. Steuertabelle)               | 13,18       |             |
| - Kirchensteuer (lt. Steuertabelle)                      | 19,17       |             |
| Summe Steuern                                            |             | 272,01      |
| - Krankenversicherung (14,6% + 0,9% = 15,5% / 2 = 7,75%) | 201,97      |             |
| - Pflegeversicherung (3,05% / 2 = 1,525 %)               | 39,74       |             |
| - Rentenversicherung (18,6% / 2 = 9,3%)                  | 242,36      |             |
| - Arbeitslosenversicherung (2,4% / 2 = 1,2%)             | 31,27       |             |
| Summe SV-Beiträge des AN                                 |             | 515,34      |
| = Nettogehalt                                            |             | 1818,65     |
| - sonstige Abzüge, z.B. Lohn-/Gehaltspfändung            |             | -           |
| +/- Vorschüsse                                           |             | -           |
| - Vermögenswirksame Leistungen (VL)                      |             | 40,00       |
| = Überweisungsbetrag                                     |             | 1778,65     |
