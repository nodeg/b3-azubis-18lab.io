---
layout: post
title:  "06. Preispolitik"
date:   2020-02-07 08:53:35 +0100
category: 2. Lehrjahr
tags: [Betriebswirtschaftliche Prozesse]
---

## Überblick - Preise festlegen

- wettbewerbsorientiert
  - Mittelpreisstrategie: Man orientiert sich am Durchschnittspreis der Wettbewerber
  - Hochpreisstrategie: Hohe Preise lassen sich mit Alleinstellungsmerkmalen durchsetzen
    - Premiumstrategie
    - Skimmingstrategie
  - Niedrigpreisstrategie: niedrige Preise können vermutet werden
    - Promotionsstrategie
    - Penetrationsstrategie
- nachfrageorientiert

## Allgemein

Kein anderes absatzpolitisches Instrument wirkt so direkt und unmittelbar wie die Preispolitik. Sowohl Kunden als auch
Wettbewerber reagieren höchst sensibel auf Preisänderungen.

Bei der Festlegung der Preise kann ein Unternehmen auf drei Verfahren zurückgreifen:

- kostenorientierte Preisfestlegung
- wettbewerbsorientierte Preisfestlegung
- nachfrageorientierte Preisfestlegung

Bei der kostenorientierten Preisfestlegung bestimmen die angefallenen Kosten den Preis. Außer den Kosten und dem
geplanten Gewinn sind keine weiteren Informationen notwendig. Bei einem derartigen Vorgehen besteht jedoch die Gefahr,
dass niemand bereit ist, das Produkt zu diesem Preis zu kaufen. Auf der anderen Seite könnte es sein, dass die Kunden
das Produkt auch zu einem wesentlich höheren Preis kaufen würden. Trotz dieser Schwächen ist die kostenorientierte
Preisfestlegung in der Praxis immer noch weit verbreitet.

Da auf den meisten Märkten eine Vielzahl gleichartiger Produkte angeboten wird, ist es sinnvoll, sich einen Überblick
über die Leistungen und Preise der Wettbewerber zu verschaffen. Bei der wettbewerbsorientierten Preisgestaltung kann man
verschiedene Strategien verfolgen.

### ABC des Marketings

Bei der Mittelpreisstrategie orientiert man sich an den Durchschnittspreisen der Wettbewerber. Hier besteht allerdings
die Gefahr, dass die Kunden das Produkt als weder qualitativ hochwertig noch als besonders preisgünstig ansehen.

Bei der Premiumstrategie, als eine mögliche Variante der Hochpreisstrategien, liegt der Preis (weit) über dem
Durchschnittspreis der Wettbewerber. Die Möglichkeit einer eigenständigen Preispolitik im Sinne einer Premiumstrategie
ist jedoch meist nur dann möglich, wenn sich das Produkt in irgendeiner Weise von den Produkten der Wettbewerber abhebt.
Wieso sollte der Kunde sonst bereit sein, einen höheren Preis zu akzeptieren? Eine weitere Hochpreisstrategie ist die
Skimmingstrategie. Bei Einführung eines neuen Produktes wird ein relativ hoher Preis festgelegt, der jedoch anders als
bei der Premiumstrategie im Laufe der Zeit nach und nach gesenkt wird. So können stufenweise große Käuferschichten
erreicht werden. Die Skimmingstrategie findet insbesondere bei technischen Produktinnovationen Anwendung.

Bei den Niedrigpreisstrategien ist zwischen der Promotionsstrategie und der Penetrationsstrategie zu unterscheiden. Bei
der Promotionsstrategie liegt der Preis (weit) unter dem Durchschnittspreis der Wettbewerber und wird auch im Laufe der
Zeit nicht verändert. Hierbei ist dafür Sorge zu tragen, dass die Kunden die Produkte nicht als qualitativ minderwertig
angesehen. Bei der Penetrationsstrategie sollen mithilfe relativ niedriger Preise viele Kunden gewonnen werden. Im Laufe
der Zeit werden die Preise dann (moderat) angehoben. Ob die Kunden ein derartiges Vorgehen akzeptieren, ist jedoch
fraglich und wird letztlich von der Attraktivität des Produktes abhängen.

Bei der nachfrageorientierten Preisfestlegung orientiert man sich an der Zahlungsbereitschaft der Kunden. Im Rahmen von
Preistests, die von beauftragten Marktforschungsinstituten durchgeführt werden, versucht man herauszufinden, zu welchem
Preis sich wie viele Produkte absetzen lassen. Im Anschluss daran kann dann unter Einbeziehung der Kosten berechnet
werden, bei welchem Preis das Unternehmen den höchsten Gewinn erwirtschaftet. Um Aufschluss über den eigenen
preispolitischen Spielraum zu bekommen, ist es für die Unternehmen sinnvoll, die Preiselastizität der Nachfrage
abschätzen zu können. Hier wird ermittelt, wie sensibel die Nachfrager auf Preissenkungen oder Preiserhöhungen bei einem
Produkt reagieren. Bei einer geringen Preiselastizität der Nachfrage reagieren die Nachfrager kauf auf Preisänderungen.
Bei einer hohen Preiselastizität der Nachfrage ist es umgekehrt.

## Preisdifferenzierung

Werden gleiche oder ähnliche Produkte zu unterschiedlichen Preisen an verschiedene Käufergruppen verkauft, spricht man
von Preisdifferenzierung. Dies ist möglich, da jede Käufergruppe persönliche Vorstellungen darüber hat, was ein Produkt
kosten darf.

Diese Vorstellungen sind nicht nur regional unterschiedlich, sondern sind beispielsweise auch vom zur Verfügung
stehenden Einkommen oder dem Verwendungszweck abhängig.

Preisdifferenzierungen versetzen Unternehmen in die Lage, das Marktpotenzial besser ausschöpfen zu können. Zudem kann
eine gleichmäßigere Kapazitätsauslastung erreicht werden. In der Praxis sind folgende Varianten anzutreffen:

- räumliche Preisdifferenzierung
- personelle (zielgruppenorientierte) Preisdifferenzierung
- zeitliche Preisdifferenzierung
- mengenmäßige Preisdifferenzierung
- verwendungsmäßige Preisdifferenzierung
- produktbezogene Preisdifferenzierung

Bei der räumlichen Preisdifferenzierung werden in verschiedenen Regionen für das gleiche Produkt unterschiedliche Preise
verlangt.

Im Rahmen der personellen (zielgruppenorientierte) Preisdifferenzierung werden bestimmte Käufergruppen Sonderpreise
eingeräumt.

Bei der zeitlichen Preisdifferenzierung werden gleiche Produkte zu verschiedenen Zeiten zu unterschiedlichen Preisen
angeboten.

Die mengenmäßige Preisdifferenzierung entspricht der Rabattgewährung.

Die verwendungsmäßige Preisdifferenzierung ist dadurch gekennzeichnet, dass je nach Einsatz des Produktes
unterschiedliche Preise verlangt werden.

Bei der produktbezogenen Preisdifferenzierung werden sehr ähnliche Produktvarianten zu unterschiedlichen Preisen
angeboten.
