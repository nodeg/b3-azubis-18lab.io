---
layout: post
title: "09. Kommunikationspolitik"
date: 2020-05-19 11:00:00 +0200
category: 2. Lehrjahr
tags: [Betriebswirtschaftliche Prozesse]
---

Im Gegensatz zur Absatzwerbung, bei der Produkte oder Produktgruppen beworben werden, und der Öffentlichkeitsarbeit,
die für den Aufbau und die Pflege eines positiven Unternehmensimages zuständig ist, soll die Verkaufsförderung
(Salespromotion) mit immer neuen und überraschenden Maßnahmen im Zusammenhang mit einem Produkt oder einer Produktgruppe
zusätzliche Kaufanreize auslösen. Die Verkaufsförderung wird somit zum verlängerten Arm der Werbung.

Hierbei ist zu beachten, dass es sich bei verkaufsfördernden Maßnahmen immer um zeitlich begrenzte und die Werbung
flankierende Maßnahmen handelt. Die Bedeutung der Verkaufsförderung ist nicht zu unterschätzen. So werden im
Durchschnitt ca. 25% des Kommunikationsetats für verkaufsfördernde Maßnahmen ausgegeben. Dies ist insbesondere auf
folgende Aspekte zurückzuführen:

- Immer mehr Produkte kommen auf den Markt und wollen verkauft werden.
- Der Druck, den Absatz zu steigern, nimmt kontinuierlich zu.
- Die Wirkung der Werbung nimmt aufgrund der permanenten Reizüberflutung ab.

Im Rahmen der Verkaufsförderung ist zwischen folgenden drei Varianten zu unterscheiden:

- Konsumorientierte Verkaufsförderung (Verbraucherpromotion)
- Handelsorientierte Verkaufsförderung (Handelspromotion)
- Verkaufspersonalisierte Verkaufsförderung (Verkäuferpromotion)

Um eine volle Wirkungsentfaltung zu erzielen, sollte nach Möglichkeit folgender Ablauf eingehalten werden.

## Konsumentenorientierte Verkaufsförderung (Verbraucherpromotion)

Verkaufsfördernde Maßnahmen, die sich direkt an den Endverbraucher oder Endanwender wenden, sind beispielsweise:

- Produktgruppen
- Zugaben
- Gewinnspiele
- Zeitlich begrenzte Preisaktionen
- Gutscheine
- Kombipack (z.B. Zahnbürste und Zahncreme) mit Preisvorteilen
- Duopack (2 gleiche Produkte) mit Preisvorteilen
- Umtauschgarantie
- Inzahlungnahme alter Waren
- Treueprämien
- Give-Aways (kleine Geschenke)


## Handelsorientierte Verkaufsförderung (Handelspromotion)

Darüber hinaus muss der Händler motiviert werden, die jeweiligen Produkte zu führen und deren Absatz wirkungsvoll zu
forcieren. Was nützt das beste Produkt, wenn der Händler ungenügend Anstrengungen unternimmt, es zu verkaufen, z.B.,
indem der Händler das Produkt ungünstig platziert oder ungenügend über die Vorteile des Produktes informiert ist?
Geeignete Maßnahmen in dem Zusammenhang sind insbesondere:

- Argumentationshilfen (Produktinformationen)
- Verkaufsschulungen für die Mitarbeiter des Händlers
- Produktvorführungen
- Befristete Preisnachlässe
- Verkaufswettbewerbe
- Anerkennung für den Händler (Geschenke, Reisen etc.)
- Finanzielle Unterstützung bei Werbemaßnahmen
- Schaufensterdekorationsservice
- Bereitstellung von Displaymaterial

## Verkaufspersonalorientierte Verkaufsförderung (Verkäuferpromotion)

Der Idee, das eigene Verkaufspersonal (Innen- und Außendienst) in die Verkaufsförderung mit einzubeziehen, liegt die
Tatsache zugrunde, dass das Personal durch sein Verhalten die Kaufentscheidung des Kunden maßgeblich beeinflusst.
Wichtige verkaufsfördernde Maßnahmenbereiche, die auch dem Bereich Fort- und Weiterbildung zugeordnet werden können,
sind:

- Ausbildung und Schulung: z.B. Produktwissen, Verkaufspsychologie, Verkaufstechniken und Verhaltenstraining.
- Motivation und Leistungsanreize: z.B. Umsatzprovision, Boni bei außergewöhnlichen Leistungen, Verkäuferwettbewerbe,
  Erfolgsbericht in Firmenzeitschrift, großzügige Spesenregelung, Anerkennung (Geschenk, Reisen, etc.), Handy, Laptop
  und Geschäftswagen.
