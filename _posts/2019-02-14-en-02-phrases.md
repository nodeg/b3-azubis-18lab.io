---
layout: post
title:  "02. Phrases"
date:   2019-02-14 11:00:00 +0100
category: 1. Lehrjahr
tags: [Englisch]
--- 

# Phrases you should know

| English                              | Deutsch                               |
| ------------------------------------ | ------------------------------------- |
| Should you have further questions... | Sollten sie weitere Fragen haben...   |
| This offer is firm/binding for...    | Dieses Angebot ist verbindlich für... |
| We would appreciate if you...        | Wir würden es begrüßen...             |
| a wide range of products             | ein breites Angebot                   |
| request for prompt delivery          | Bitte um sofortige Lieferung          |
| on receipt of invoice                | nach Erhalt der Rechnung              |
| please find enclosed                 | in der Anlage finden Sie              |
| We are pleased...                    | Wir freuen uns...                     |
