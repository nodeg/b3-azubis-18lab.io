---
layout: post
title: "15. Break-Even-Point"
date: 2021-02-02 09:50:17 +0200
category: 3. Lehrjahr
tags: [ Betriebswirtschaftliche Prozesse ]
---

> Der Break-Even-Point wird auch kritische Menge oder Gewinnschwelle genannt.

Ein Unternehmen kalkuliert mit 5000,00€ Fixkosten, die variablen Stückkosten betragen 100,00€.

k_f = fixe Gesamtkosten
k_v / K_v = variable Gesamtkosten
k_g Gesamtkosten (SK

Stellen Sie den Sachverhalt der Gesamtkosten bei einer Kapititätsgrenze von 150 Stück grafsch dar! Dazu zeichnen Sie zuerst die Fixkosten, dann die variablen Kosten und am Schluss die Gesamtkosten (=fixe + variable Kosten) ein.

(Hinweis: x-Achse: 10 Stück = 1 Kästchen; y-Achse: 1000€ = 1 Kästchen)

Angemmen Sie können einen Marktpreis von 150,00€ erzielen:

a) Stellen Sie die Umsatzerköse graphisch in der Zeichnung aus Aufgabe 1 dar.
b) Ab welcher Produktionsmenge schreiben Sie schwarze Zahlen? --> Break-Even-Point bei 100 Stück
c) Die kritische Menge lässt sich auch rechnerisch bestimmen:

Umsatzerlöse = Kosten
Verkaufspreis (p) * Stückzahl (x) = Fixkosten (K_r) + variable Stückkosten (k_v) * Stückzahl (x)

$$\rightarrow x = k_f / (p - k_v) = k_f / db = 5000 / (150-100) = 500 / 50 = 100$$

150 * x = 5000 + 100 * x
50x = 5000
x = 100
