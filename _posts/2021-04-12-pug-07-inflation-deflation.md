---
layout: post
title:  "07. Inflation & Deflation"
date:   2020-12-15 06:00:00 +0100
category: 3. Lehrjahr
tags: [Politik und Gesellschaft]
---

## Inflation

... das heißt einfach gesagt: Geld wird entwertet

Nachfrage sinkt
=> Preise steigen
=> Geldwert sinkt

Ursachen der Inflation:

- Nachfrageinflation: Es entsteht eine Preissteigerung, weil Konsumenten häufiger nach bestimmten Gütern
- Angebotsinflation
- Importierende Inflation: Durch Export steigt die Geldmenge im Inland

Merkmale des Geldwertes:

- Preise steigen rasch, Löhne und sonstige Einkommen steigen langsam und zeitversetzt
- Geldanlagen verlieren an Wert, Sachanlagen gewinnen an Wert
- Lohnerhöhungen zum Ausgleich der Preissteigerung führen zu gering steigenden Nettolöhnen aufgrund der
  Steuerprogression und der Sozialabgaben

## Deflation

... das heißt einfach gesagt: Geld gewinnt an Wert/Kaufkraft steigt

Nachfrage sinkt
=> Preise sinken
=> Unternehmensgewinne sinken
=> Arbeitslosigkeit steigt
=> Kaufkraft sinkt
=> Nachfrage sinkt noch mehr

Ursachen der Deflation:

- allgemeine Konsum und Investitions-Zurückhaltung: tritt häufig auf, wenn eine Verschlechterung der Wirtschaftlage
  erwartet wird. Verbraucher sind dann vorsichtiger bei Ausgaben und Unternehmen investieren weniger. Dementsprechend
  sinkt die Güternachfrage.
- weniger Nachfrage aus dem Ausland: bei exportintensiven Volkswirtschaften schlägt eine sinkende Exportnachfrage
  infolge einer international verschlechterten Wirtschaftslage

- anhaltende Preisrückgänge führen bei Unternehmen zu sinkenden Gewinnen
- sinkende Arbeitslosigkeit bewirkt höheres Einkommen und höhere Nachfrage
- Sachvermögen verliert wegen geringer Nachfrage an Wert
- geringe Nachfrage lässt Preise ansteigen => dies erhöht den Kostendruck in den Unternehmen => die Arbeitslosigkeit
  sinkt weiter
