---
layout: post
title: "01. Entscheidungstabellen"
date: 2020-11-06 08:30:00 +0200
category: 3. Lehrjahr
tags: [Anwendungsprogrammierung]
---

## Zweck und formaler Aufbau

Entscheidungstabellen sind eine formale Darstellung von Wenn-Dann-Problemen. Sie sind ein grafisches Hilfsmittel zur
Darstellung komplexer Entscheidungszusammenhänge zwischen Bedingungen und Aktionen in kompakter und übersichtlicher
Form.

Die Anwendung ist nicht auf den IT-Bereich begrenzt. Beispielsweise enthalten juristische Texte zahlreiche Bedingungen
und Aktionen. Typische Anwendungsbeispiele sind:

- Berechnungsmethoden (Rabatt- / Skontogewährung)
- Arbeitsanweisungen (Montageanleitung)
- Formalisierbare Entscheidungsprozesse aller Art (Ampelsteuerung)

Entscheidungstabellen werden zur Problemlösung und zur Dokumentation eingesetzt. Das Ziel des ET-Einsatzes ist es, die
Kommunikation zu erleichtern. Daher ist es sinnvoll, bei der Aufstellung von Entscheidungstabellen allgemein gültige
Formvorschriften der DIN 66241 zu beachten.

### Struktur

| | Name der ET | Regelnummern |
| - | --------- | ------------ |
| wenn | Bedingung | Bedingungszeiger |
| dann | Aktionenen | Aktionszeiger |

### Verarbeitungsregeln

Das Anzeigefeld (Bedingungsanzeiger und Aktionsanzeiger) wird in Spalten aufgeteilt. Die Anzeige-Elemente in einer
Spalte kennzeichnen die betreffenden Bedingungskonstellationen und zugehörige Aktionsfolgen. Beide zusammen werden als
Regeln bezeichnet. Die Regeln einer ET sind fortlaufend zu nummerieren.

Beispiel: Überholen

Gemäß der Straßenverkehrsordnung darf nur dann überholt werden, wenn die Gegenfahrbahn frei und die Straße übersichtlich
ist. Stellen Sie in Abhängigkeit von diesen Bedingungen die Aktionen eines Kraftfahrzeugs dar.

Entwerfen Sie eine nach der systematischen Methode erstellte ET:

| Überholen            | R1 | R2 | R3 | R4 |
| -------------------- | -- | -- | -- | -- |
| Gegenfahrbahn frei   | J  | J  | N  | N  |
| Straße übersichtlich | J  | N  | J  | N  |
| Überholen            | X  |    |    |    |
| Nicht überholen      |    | X  | X  | X  |

## Arten

### ET mit begrenzter Anzeige

Für den Bedingungsanzeiger gibt es maximal drei Möglichkeiten:

- JA - ``J``
- NEIN - ``N``
- Irrelevant (don't care) - ``-``

Die maximale Zahl der Regeln in einer ET ist 2^B, wobei B für die Anzahl der Bedingungen steht. Die Anzahl der möglichen
Aktionen hat aber keinen Einfluss auf die Anzahl der Regeln!

Beispiel: Bewertung von Hotels

Hotels an der Mittelmeerküste werden nach folgenden vier Bedingungen geprüft:
Strandnähe - ruhige Lage - deutsche Küche - deutschsprachige Bedienung

- Alle vier Bedingungen erfüllt ==> Kategorie A
- Drei Bedingungen erfüllt ==> Kategorie B
- Zwei Bedingungen erfüllt ==> Kategorie C
- Eine oder keine Bedingung erfüllt ==> streichen

| Hotelklassen     | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 |
| ---------------- | - | - | - | - | - | - | - | - | - | -- | -- | -- | -- | -- | -- | -- |
| B1 Strandnähe    | j | j | j | j | j | j | j | j | n | n  | n  | n  | n  | n  | n  | n  |
| B2 Ruhige Lage   | j | j | j | j | n | n | n | n | j | j  | j  | j  | n  | n  | n  | n  |
| B3 Dt. Küche     | j | j | n | n | j | j | n | n | j | j  | n  | n  | j  | j  | n  | n  |
| B4 Dt. Bedienung | j | n | j | n | j | n | j | n | j | n  | j  | n  | j  | n  | j  | n  |
| A1 Kategorie A   | X |   |   |   |   |   |   |   |   |    |    |    |    |    |    |    |
| A2 Kategorie B   |   | X | X |   | X |   |   |   | X |    |    |    |    |    |    |    |
| A3 Kategorie C   |   |   |   | X |   | X | X |   |   | X  | X  |    | X  |    |    |    |
| A4 Streichen     |   |   |   |   |   |   |   | X |   |    |    | X  |    | X  | X  | X  |

- Vollständige Entscheidungstabelle, nach der systematischen Methode erstellt. => Sämtliche Elemente im
  Bedingungsanzeiger sind immer zweiwertig (j oder n).
- Die notwendige Anzahl der Entscheidungsregeln R1 .. Rn ist immer: n=2B, d.h. die Hereinnahme einer zusätzlichen
  Bedingung B verdoppelt sofort die - meist ohnehin schon große - Anzahl der notwendigen Entscheidungsregeln.
  => Möglichst eine geringe Zahl an Bedingungen anstreben!
- Da keine Gewichtung der Bedingungen erfolgt, sind diese voneinander unabhängig.

Viel häufiger kommt es vor, dass einzelne Bedingungen logisch voneinander abhängig sind. Man bezeichnet solche
Bedingungen als "Ausschlussbedingungen": Das Zutreffen einer Bedingung schließt das gleichzeitige Zutreffen einer
anderen aus.

Solche Ausschlussbedingungen sind beispielsweise:
- Altersgruppe
- Gehaltsstufe
- Lohnsteuerklasse
- Beitragssätze

Ein Mitarbeiter kann nicht gleichzeitig verschiedenen Altersgruppen angehören, und auf einer Lohnsteuerkarte können
nicht gleichzeitig verschiedene Steuerklassen stehen.

### Vollständigkeitsprüfung

Die Tabelle ist formal vollständig, wenn aufgrund der Bedingungen genügend Regeln erfasst wurden. (Vorsicht, wenn
Redundanzen oder Widersprüche vorliegen)

Maximale Regelanzahl n = 2^B

Jeder Irrelevanzanzeiger steht für 2 Möglichkeiten (J oder N). Die Zahl der erfassten Regeln ist daher größer als die
Zahl der abgebildeten Regeln in obigem Beispiel. Für jede Regel ist also die 2 mit der Zahl der Irrelevanzanzeiger zu
potenzieren.

In einem Beispiel mit vier Regeln, bei der zwei Regeln mit don't cares versehen werden: n = 2^3 = 2 * 2 * 2 = 8

Regel 1: 2^2 = 4
Regel 2: 2^1 = 2
Regel 3: 2^0 = 1
Regel 4: 2^0 = 1

Die Zahl der erfassten Regeln (4+2+1+1 = 8) stimmt also mit der Zahl der maximal möglichen Regeln überein.

=> Die ET ist formal vollständig!

## Redundanz und Konsolidierung

Durch eine Zusammenfassung von 2 Regeln kann man eine ET konsolidieren. Dies ist immer dann möglich, wenn

1. zwei Regeln die gleiche Aktion bewirken,
2. zwei Regeln sich in nicht mehr als einem Bedingungsanzeiger unterscheiden.

Beispiel:

| Kreditgewährung                        | R1  | R2  | R3  | R4  |
| -------------------------------------- | --- | --- | --- | --- |
| B1 monatliches Einkommen > Kreditlimit | N   | N   | J   | J   |
| B2 Zahlungsverhalten bislang gut       | N   | J   | N   | J   |
| A1 Kredit geben                        |     |     | X   | X   |
| A2 Vorgesetzter entscheidet            |     | X   |     |     |
| A3 Kein Kredit                         | X   |     |     |     |

In der oben gesehen Tabelle können R3 und R4 zusammengefasst werden. Das Ergebnis der zusammengefassten Tabelle sieht
dann so aus:

| Kreditgewährung                        | R1  | R2  | R3  |
| -------------------------------------- | --- | --- | --- |
| B1 monatliches Einkommen > Kreditlimit | N   | N   | J   |
| B2 Zahlungsverhalten bislang gut       | N   | J   | -   |
| A1 Kredit geben                        |     |     | X   |
| A2 Vorgesetzter entscheidet            |     | X   |     |
| A3 Kein Kredit                         | X   |     |     |

Redundanz bedeutet, dass die Regeln, die die gleiche Bedingungsanzeiger-Kombination und die gleichen Aktionen
beinhalten, öfter als einmal in der ET vorkommen. Weist eine Entscheidungstabelle mehr Regeln auf als notwendig, sind
möglicherweise Regeln redundant.  
Da die Regeln gleiche Aktionen haben, kann man durch Konsolidierung jeweils zwei Regeln zu einer zusammenfassen.
Redundanz liegt auch vor, wenn eine Regel vollständig in einer anderen Regel enthalten ist (R1 ist redundant):

| R1  | R2  | R1,2 |
| --- | --- | ---- |
| J   | -   | -    |
| J   | J   | J    |
| J   | J   | J    |
| X   | X   | X    |

## Widerspruch

Ein Widerspruch liegt vor, wenn inhaltlich gleiche Bedingungen zu unterschiedlichen Aktionen führen. Ein solcher
Widerspruch wird oft erst durch das Erstellen einer ET aufgedeckt, weil im zu Grunde liegenden Text die Bedingungen
verschieden formuliert waren und daher nicht auf den ersten Blick als inhaltlich gleich erkannt wurden.

Beispiel:

| Beispiel | R1 | R2 | R3 |
| -------- | -- | -- | -- |
| B1       | N  | J  | J  |
| B1       | J  | J  | J  |
| B1       | N  | J  | -  |
| A1       | X  |    | X  |
| A2       | X  | X  |    |

Regel 2 und Regel 3 beinhalten gleiche Bedingungskonstellationen (J/J/J), aber unterschiedliche Aktionen (Regel 2 -->
Aktion 2 und Regel 3 --> Aktion 1).

=> Zwischen Regel 2 und Regel 3 liegt ein Widerspruch vor
