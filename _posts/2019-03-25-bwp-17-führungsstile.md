---
layout: post
title:  "17. Führungsstile"
date:   2019-03-25 18:00:00 +0100
category: 1. Lehrjahr
tags: [Betriebswirtschaftliche Prozesse]
---

# Führungsstile

|             | Autoritär                                                    | Kooperativ                                                   | Laissez faire                                                |
| ----------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Kennzeichen | Vorgesetzter<br>- Entscheidet alleine<br>- Erteilt Befehle an MA<br>- Kontrolliert MA | Vorgesetzter<br>- Entscheidet mit dem MA<br>- Gewährt Freiräume<br>- Greift ein, wenn nötig | Vorgesetzter<br>- Kontrolliert MA nicht<br>- Zieht keine Konsequenzen<br>- Achtet nicht auf Effizienz |
| Vorteile    | - Klare Regelungen und Aufgabenverteilung <br>- Schnelle Entscheidung<br>- MA ohne Verantwortung (kann auch Nachteil sein) | - MA sind am Entscheidungsprozess beteiligt<br>- MA bringen Kompetenzen stärler ein | - Ma werden nicht kontrolliert<br>- Freie Entfaltung         |
| Nachteile   | - Mitarbeiter fühlen sich unwohl/demotiviert<br>- Kompetenzen werden nicht optimal genutzt | - Kompetenzüberschneidungen bzw. Streitigkeiten<br>- Qualifiziertes Personal $\rightarrow$ Kosten | - Planlos/unkontrolliertes Arbeiten<br>- Gleichgültigkeit    |
