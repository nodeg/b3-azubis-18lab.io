---
layout: post
title: "04. Virtual Private Networks"
date: 2020-12-10 08:30:00 +0200
category: 3. Lehrjahr
tags: [IT-Systeme]
---

## Grundlagen

Bsp.: Alice & Bob

- Vertraulichkeit: Niemand anderes als Bob kann die Nachricht im Klartext lesen, wenn Alice die Nachricht sendet. --> Verschlüsselung der Nachricht
- Authentizität der Quelle: Bob kann überprüfen das die Nachricht von Alice kommt. --> Signatur der Nachricht
- Datenintegrität: Bob kann überprüfen das die Nachricht von Alice nicht manipuliert worden ist. --> Hashing der Nachricht

## Architekturen

Der Aufbau eines Virtual-Private-Network hängt insbesondere vom Einsatzszenario ab.
Jedes VPN besteht aus drei Elementen

- VPN-Client (baut die Verbindung auf),
- VPN-Server (nimmt die Verbindung entgegen) und der
- VPN-Tunnel (logische Verbindung über ein Netzwerk). 

Für die Sicherheit der Verbindung ist das VPN-Protokoll (z.B. IPSec) zuständig. Dieses muss von den
Kommunikationspartnern unterstützt werden. Das nächste Kapitel beschäftigt sich mit den Eigenschaften und der Auswahl
eines geeigneten VPN-Protokolls.

### Client-Server VPN

Ein Außendienstmitarbeiter verbindet sich vom Notebook über das Internet mit der Firmenzentrale. Diese Variante ist das
sogenannte Client-to-Site-VPN. Andere Namen dafür sind Access-VPN oder User-to-Site-VPN. Auf dem Notebook muss dazu ein
VPN-Client (Software) installiert sein. VPN-Server ist in diesem Beispiel der VPN-Router, er beherrscht das gleiche
VPN-Protokoll.

![ITS - Client-Server-VPN](/assets/img/ITS/04-Client_to_Site_VPN_2.png)

### Server-Server VPN

Werden zwei Standorte, bzw. Netzwerke über ein öffentliches Netzwerk (Internet) miteinander verbunden, spricht man von
einem Site-to-Site-VPN. Dieses eignet sich dann, wenn z.B. die Filiale einer Firma an die Zentrale angebunden wird. In
diesem Fall müssen nur die beiden Gateways (i.d.R. VPN-fähige Router) das gleiche VPN-Protokoll sprechen und zueinander
einen VPN-Tunnel aufbauen.

![ITS - Server-Server-VPN](/assets/img/ITS/04-Site_to_Site_VPN_1.png)

### Client-Client VPN

Ein Host-to-Host-VPN ermöglicht den sicheren Datenaustausch zum Beispiel zwischen zwei Rechnern oder Servern.

![ITS - Server-Server-VPN](/assets/img/ITS/04-Host-to-Host-VPN_1.png)

## Protokolle

### Merkmale und Einordnung

Die Grundlage für ein Virtual-Private-Network ist das VPN-Protokoll. Es definiert das Zusammenspiel von VPN-Client und
VPN-Server, den Verbindungsauf- und abbau. Darüber hinaus legt ein VPN-Protokoll die Art der Verschlüsselung, die
Authentifizierung und Maßnahmen zur Sicherung der Datenintegrität fest. Die beteiligten VPN-Partner müssen das gleiche
VPN-Protokoll unterstützen. Die Auswahl eines geeigneten Verfahrens ist von verschiedenen Anforderungen abhängig.

Beispiele:

- VPN-Architektur
- Art der Endgeräte und Gateways (Server, Router)
- Betriebssysteme und Kompatibilität
- Schutzbedarf
- Wartbarkeit und administrativer Aufwand

VPN-Protokolle lassen sich den OSI-Schichten zuordnen:

| Protokoll (Auswahl) | OSI-Schicht |
| ------------------- | ----------- |
| PPTP, L2TP          | 2           |
| IPSec               | 3           |
| SSL/TLS, OpenVPN    | 5-7         |

### Beispiele für VPN-Protokolle: PPTP und IPSec

#### PPTP (Point-to-Point Tunneling Protocol, RFC 2637)

Dieses Protokoll wird primär von Microsoft unterstützt. Hinter der Funktion "sichere Verbindung zum Arbeitsplatz" eines
Microsoft-Betriebssystems verbirgt sich PPTP. Dieses VPN-Protokoll verbindet unterschiedliche Teilprotokolle bzw.
Funktionen. MPPE (Microsoft-Point-to-Point Encryption) arbeitet auf Basis von RC4 und dient als
Verschlüsselungsverfahren. Die Authentifizierung erfolgt z.B. über CHAP bzw. MS-CHAP (Challenge Authentication Protocol).

Bereits 2012 hat die c't nachgewiesen, dass PPTP angreifbar ist. In sicherheitskritischen Umgebungen oder im
Firmeneinsatz sollte PPTP nicht mehr eingesetzt werden.

#### IPSec (Internet Protokoll Security, RFC 4301)

IPSec wurde entwickelt um alle Anforderungen an eine sichere Datenübertragung zu erfüllen. Es soll kritische ältere
VPN-Protokolle ablösen, war aber lange unbeliebt, da es insbesondere zwischen Systemen unterschiedlicher Hersteller
schwierig einzurichten ist. Diese Probleme sind weitgehend behoben, IPSec wird damit interoperabel und flexibel
einsetzbar.

Der hohe Sicherheitsanspruch hat aber seinen Preis: Müssen alle Größen einer IPSec-Verbindung manuell eingestellt
werden, kann dies sehr aufwändig werden. Assistenzsysteme unterstützen die Anwender in diesem Punkt und VPN-Partner
(VPN-Client und -Server, auch VPN-Peers genannt) handeln Verfahren zur Verschlüsselung, Authentifizierung und
Datenintegrität selbstständig aus.

> Hinweis: AES und 3DES sind symmetrische Verschlüsselungsverfahren, SHA1, SHA2 und MD5 sind Hash-Verfahren.

### IPSec im Detail

IPSec ist Bestandteil von IPv6, kann aber auch unter IPv4 eingesetzt werden. Es ist inzwischen eine der wichtigsten
VPN-Protokolle. Um möglichst viele Einsatzszenarien abbilden zu können, bietet IPSec zwei unterschiedliche Betriebsmodi:
Transportmodus und Tunnelmodus.

#### Tunnelmodus

Wird ein VPN zwischen zwei Gateways eingerichtet, kann dies im Tunnelmodus realisiert werden. Dabei wird das komplette
IP-Paket verschlüsselt (ESP, s.u.) und mit einem neuen IP-Header und einem IPSec-Header versehen. Dadurch ist das
IPSec-Paket größer als im Transportmodus. Der Vorteil besteht hier darin, dass in den LANs, die zu einem VPN verbunden
werden sollen, die Gateways so konfiguriert werden können, dass sie IP-Pakete in IPSec-Pakete umwandeln und dann über
das Internet dem Gateway im Zielnetzwerk zusenden. Dieses stellt das ursprüngliche Paket wieder her und leitet es
weiter. Dadurch wird eine Neukonfiguration der LANs umgangen, da nur in den Gateways IPSec implementiert sein muss.
Außerdem können Angreifer so nur den Anfangs- und Endpunkt des IPSec-Tunnels feststellen. Bei einem Site-to-Site-VPN
wird IPSec im Tunnelmodus eingesetzt.

#### Transportmodus

Im Transportmodus (mit ESP) verschlüsselt IPSec nur den Datenteil des zu transportierenden IP-Paketes. Der
Original-IP-Header bleibt dabei erhalten und es wird ein zusätzlicher IPSec-Header hinzugefügt.

Der Vorteil dieser Betriebsart ist, dass jedem Paket nur wenige Bytes hinzugefügt werden. Angreifern ist es jedoch
möglich, den Datenverkehr im VPN zu analysieren, da die IP-Header nicht modifiziert werden. Die Daten selbst sind aber
verschlüsselt, sodass man nur feststellen kann, welche Stationen wie viele Daten austauschen, aber nicht welche Daten.
Der Transportmodus wird verwendet, wenn Stationen (Client/Client, Client/Server, Server/Server) direkt über eine
gesicherte Verbindung kommunizieren müssen. Diese Stationen müssen als Teil des VPN IPSec unterstützen.

Diese Betriebsart entspricht einem Host-to-Host-VPN.

![ITS - IPSec-Paket im Transportmodus](/assets/img/ITS/04-IPSec_ESP_Transportmodus.png)

IPSec nutzt unterschiedliche Sicherheitsprotokolle:

- Authentication Header, AH und
- Encapsulation Security Payload, ESP

Welches Verfahren zum Einsatz kommt, ist vom Sicherheitsbedarf abhängig. Die Einstellungen sind im IPSec-Header
definiert.

#### Authentication Header, AH

Der Authentication-Header sichert die Integrität (Verhinderung der Datenveränderung auf dem Übertragungsweg) und
Authentizität der Daten und der statischen Felder des IP-Headers.

Der AH bietet keinen Schutz der Vertraulichkeit und benutzt keine digitale Signatur, da diese Technik rechenintensiv ist
und den Datendurchsatz im VPN stark reduzieren würde. Mit anderen Worten: Die Daten werden nicht verschlüsselt.

#### Encapsulation Security Payload, ESP

Der ESP schützt die Vertraulichkeit, die Integrität und Authentizität von Datagrammen. Es bietet damit die
Sicherheitsfunktionen von AH und zusätzlich die Verschlüsselung der Daten, bzw. des Original-IP-Paketes. Er schließt
aber die statischen Felder des IP-Headers bei einer Integritätsprüfung nicht ein.

In den meisten Fällen wird man auf die Verschlüssung der Daten nicht verzichten können und damit ESP statt AH einsetzen.

![ITS - IPSec-Paket im Tunnelmodus mit ESP](/assets/img/ITS/04-IPSec_ESP_Tunnelmodus.png)

#### NAT-Traversal

Auf der WAN-Schnittstelle des VPN-Routers für den Internetzugang ist in der Regel die NAT-Funktion aktiviert. Da
eingehende VPN-Verbindungen verschlüsselt sind, können diese nicht verarbeitet werden. Die NAT-Funktion (NAT-Firewall)
würde die Daten verwerfen. NAT-Traversal (NAT-T) ermöglicht das "Durchreichen" der Verbindung zur VPN-verarbeitenden
Instanz im Router. IPSec nutzt zum Verbindungsaufbau UDP mit dem Port 500 / 4500.

> "Ohne NAT-T kann es zwischen IPSec und NAT zu Inkompatibilitäten kommen (siehe RFC 3715, Abschnitt 2). Diese behindern
> vor allem den Aufbau eines IPSec-Tunnels von einem Host innerhalb eines LANs und hinter einem NAT-Gerät zu einem
> anderen Host bzw. Gerät. NAT-T ermöglicht derartige Tunnel ohne Konflikte mit NAT-Geräten, aktiviertes NAT wird vom
> IPSec-Daemon automatisch erkannt und NAT-T wird verwendet."
> 
> Hilfe, bintec-Router
