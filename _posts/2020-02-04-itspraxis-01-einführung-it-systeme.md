---
layout: post
title:  "01. Einführung in IT-Systeme"
date:   2019-02-13 06:00:00 +0100
category: 1. Lehrjahr
tags: [IT-Systeme, Praxis]
--- 

# Einführung in IT-Systeme

![ITS - Rechner Aufbau](/assets/img/ITS/01-rechner-aufbau.jpg)

Komponenten:

- CPU/Prozessor: Central Processing Unit/Zentrale Steuerung und Ausführung
- GPU/Grafikkarte: Graphical Processing Unit/Berechnung von visueller Ausgabe
- RAM/Arbeitsspeicher: Random Access Memory/nicht persistenter Zwischenspeicher (auch Hauptspeicher genannt)
- Festplatten: HDD und SSDs sind für die Persistierung des Speichers und der Daten zuständig.
- Netzteil: Stromversorgung des Systems
- Laufwerk: DVDs/Blu-Rays/Disketten/Magnetband sind IO Medien die für Ein und Ausgabe zuständig sind.
- Gehäuse: Schutz der Komponenten, sowie Kabelmanagement und Halterung für die Komponenten
- Kühlung: Luft-, Wasser oder Stickstoffkühlung dient dazu das System im Temperaturbereich zu halten in dem keine
           kritische Komponente Schaden nimmt.
- Mainboard/Hauptplatine: Verbindet die Komponenten und hat heutzutage Dinge wie Chipsatz und LAN, sowie weitere
                          IO-Ports mit On-Board

Quelle: [https://www.ingame.de/bilder/2016/02/01/12785978/2019649607-wie-stelle-ich-mir-einen-rechner-zusammen-teil-1-begriffe-und-komponenten-thumbnail2-Pgef.jpg](https://www.ingame.de/bilder/2016/02/01/12785978/2019649607-wie-stelle-ich-mir-einen-rechner-zusammen-teil-1-begriffe-und-komponenten-thumbnail2-Pgef.jpg)
