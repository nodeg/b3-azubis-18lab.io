---
layout: post
title:  "05. Konjunktur"
date:   2020-12-09 06:00:00 +0100
category: 3. Lehrjahr
tags: [Politik und Gesellschaft]
---

## Konjunkturverlauf

Die konjunkturelle Entwicklung vollzieht sich in Zyklen. Der Konjunkturaufschwung (Expansion) ist durch steigende
Nachfrage und Produktionserweiterung gekennzeichnet. Die Wirtschaftseinheiten sind optimistisch. Die Firmen investieren
und brauchen zusätzliche Arbeitskräfte. In der Hochkonjunktur (Boom) steigen die Löhne und das Einkommen der Haushalte,
denn es mangelt schon an Arbeitskraft. Die steigende Nachfrage auf dem Gütermarkt treibt die Preise, auf dem Kreditmarkt
die Zinssätze in die Höhe. Auch die Notenbank will die Konjunktur bremsen und erhöht die Zinsen. Das führt zu einem
Konjunkturabschwung (Rezession), der durch rückläufige Produktion und unausgelastete Kapazitäten gekennzeichnet ist.
Die Unternehmererwartungen sind pessimistisch, Kapazitäten werden abgebaut, Arbeitnehmer werden entlassen. Im
Tiefstand (Depression) ist der Staat durch die Transferzahlungen an Arbeitslose und durch die sinkenden Steuereinkommen
besonders belastet. Die Notenbank signalisiert durch die niedrigen Zinssätze einen neuen Aufschwung. Das Geld wird
billiger, staatliche Förderungsgelder sollen die Wirtschaft ankurbeln. Wenn der untere Wendepunkt (psychologische
Talsohle) erreicht ist, werden auch die Erwartungen optimistischer und fängt ein neuer Aufschwung an.

Die konjunkturellen Schwankungen kann man auch innerhalb eines Jahres feststellen. Diese kurzfristigen Veränderungen,
die sogenannten Saisonschwankungen sind auf saisonale Einflüsse (witterungsbedingte Einflüsse, Urlaubsmonate,
Weinachten, usw.) zurückzuführen. Die Kurve der Konjunkturschwankungen zeigt die jährliche Entwicklung des
Bruttosozialproduktes (BSP). Die Trendlinie charakterisiert die langfristige Entwicklung, meistens den langfristigen
Anstieg einer Volkswirtschaft.

![PuG - Konjonkturverlauf Trendlinine](/assets/img/pug/05-konjunktur-1.png)

## Konjunkturbewegungen

Der Wirtschaftsablauf in der Marktwirtschaft ist nie gleichmäßig. Betrachtet man die Wirtschaftsentwicklung in Ländern
mit marktwirtschaftlicher Ordnung während der letzten 100 Jahre, so fällt auf, dass wirtschaftliche Hochs mit Tiefs
regelmäßig wechselten. Das Wirtschaftsverhalten ist somit zyklisch. Bei wiederkehrenden kurzfristigen Änderungen der
Wirtschaftslage spricht man von Saisonschwankungen, die meist nur einzelne Wirtschaftszweige betreffen, z. B. das
Baugewerbe infolge ungünstiger Witterung. Auf die gesamte Volkswirtschaft bleiben sie ohne großen Einfluss. Dagegen
wirken sich die über mehrere Jahre hin wirksamen Wirtschaftsschwankungen auf die ganze Wirtschaft aus. Extreme
Wellenbewegungen schaden dem empfindlichen Organismus einer Wirtschaft. Deshalb werden vom Staat Maßnahmen der Kontrolle
ergriffen.

### Staatliche Maßnahmen zur Konjunkturbelebung

Staatlichen Eingriffen in den Wirtschaftsablauf sind in der Bundesrepublik Grenzen gesetzt. Der Staat kann niemandem
vorschreiben, welche wirtschaftlichen Entscheidungen zu treffen sind. Er kann nur Vergünstigungen und Anreize bieten
oder finanzielle Beteiligungen vorsehen. Den privaten Haushalten bleibt es überlassen, ob sie davon Gebrauch machen.
Lange Zeit galt in der BRD der Budgetausgleich zwischen Steuereinnahmen und -ausgaben. Das bedeutet: Der Staat gibt in
Krisenzeiten, wenn er weniger Steuereinnahmen hat, auch weniger aus - was natürlich die Krise noch verschärft! Diese
Ansicht hat sich als überholt erwiesen. Heute wird vom Staat antizyklisches Verhalten erwartet. Im Interesse der
Konjunkturstabilisierung muss er in Zeiten unzureichender Gesamtnachfrage die Staatsausgaben steigern.

Was der Staat selbst verbraucht oder als „Unternehmer“ an Investitionen tätigt, macht in einer sozialen Marktwirtschaft
unter günstigen Bedingungen rund 20 Prozent der Gesamtnachfrage im Land aus. Er ist so bereits ein wichtiger Mitspieler
auf dem Markt und kann ihn dadurch beeinflussen. So kann der Staat bei einem Rückgang der allgemeinen
Wirtschaftstätigkeit seine Ausgaben erhöhen, zum Beispiel durch Bauaufträge. Finanziert werden solche Aufträge
vorausplanend während einer Hochkonjunktur. Er spart sich dieses Geld für schlechtere Zeiten auf.

Darüber hinaus kann der Staat durch Subventionen in Form von Lohnkostenzuschüssen zur Einstellung von Arbeitskräften
beitragen. In der Rezession kann der Staat die privaten Investitionen fördern, indem er den Unternehmen Investitionsboni
einräumt, das heißt, ein bestimmter Prozentsatz der Anschaffungskosten der Investitionsgüter kann von der Steuerschuld
abgezogen werden. Allgemein kann der Staat durch steuerliche Vergünstigungen der Wirtschaft in Krisenzeiten Anreize für
Investitionen bieten. Durch Vereinfachungen von Gesetzen und Vorschriften besteht die Möglichkeit, weitere Anreize für
die Schaffung von neuen Arbeitsplätzen bieten. So wirkt sich eine Senkung von Sicherheitsstandards ebenso positiv aus,
wie die Lockerung des Kündigungsschutzes.

| Möglichkeiten staatlicher Konjunkturpolitik          |                                                               |
| ---------------------------------------------------- | ------------------------------------------------------------- |
| Nachfragesteuerung durch ...                         | Angebotssteuerung durch ...                                   |
| Stärkung der Investitionen, z.B. Steuerentlastung, mehr direkte Subventionen | Entlastung der Unternehmen, z.B. Senkung von Sozialabgaben, Lohnnebenkosten und Steuern |
| Stärkung des privaten Verbrauchs, z.B. Senkung der Lohn- und Einkommenssteuer, Ausweitung der Sozialleistungen, Subventionen (z.B. Solaranlagen, Heizungssanierung) | Vereinfachung von Steuergesetzen, Vorschriften |
| Erhöhung der Staatsausgaben, z.B. Konjunkturprogramme durch mehr öffentliche Aufträge | Lockerung sozialer Schutzvor-schriften, z.B. Kündigungsschutz |
|                                                      | Erleichterungen von Unternehmens-gründungen                   |
|                                                      | Förderung von Wissenschaft, Bildung und Schlüsseltechnologien |
| Kritik:                                              |                                                               |
| Staatsverschuldung steigt zunehmend, da diese in Phasen der Hochkonjunktur nicht abgebaut wird. | Belastung der Arbeitnehmer ist keine Garantie dafür, dass Arbeitsplätze erhalten oder zusätzlich geschaffen werden. |

### Aktivitäten des Staates in Hochkonjunkturphasen

Auch in Zeiten von Expansion und Hochkonjunktur findet man wieder die antizyklische Finanzpolitik. Im Aufschwung und
Boom ist die Nachfrage nach Konsumgütern durch die privaten Haushalte sehr groß, auch die Unternehmen investieren
vorwiegend in diesen Zeiten in ihre Betriebe. Es besteht dadurch die Gefahr von Preissteigerungen, da das Angebot nicht
in gleichem Maße steigt.

Durch Kürzung der Staatsausgaben können Preissteigerungen verringert oder vermieden werden. Dies kann durch Streichung
von Subventionen für krisengebeutelte Branchen geschehen, aber auch beispielsweise durch Kürzung von
Arbeitsbeschaffungsmaßnahmen.

Durch Steuererhöhungen werden nicht nur die Wirtschaft und damit der Preisanstieg gedämpft, sondern der Staat kann
dadurch auch Rücklagen bilden, um in schlechteren Zeiten entsprechend auf die Konjunktur reagieren zu können.

### Antizyklischen Finanzpolitik

Konjunkturelle Schwankungen können gemildert werden, wenn eine rechtzeitige Steuerung der gesamtwirtschaftlichen
Nachfrage durch finanzpolitische Maßnahmen des Staates entgegengesetzt zum Konjunkturverlauf erfolgt (antizyklische
Finanzpolitik). Im Abschwung muss der Staat deshalb die Nachfrage beleben und z. B. durch Erhöhung der Ausgaben für
öffentliche Projekte, Subventionen oder Steuersenkungen die Investitionsbereitschaft der Unternehmen erhöhen und den
privaten Konsum stimulieren. Im Boom soll der Staat entsprechend die Nachfrage z. B. durch Steuererhöhungen oder
Senkungen seiner Ausgaben für öffentliche Aufträge dämpfen, um eine Überhitzung der Konjunktur zu verhindern. Die
Belebung der Nachfrage im Abschwung sollte nach dieser Auffassung auch dann erfolgen, wenn die höheren öffentlichen
Ausgaben über höhere Schulden finanziert werden müssen (Defizitfinanzierung). Die im Abschwung entstandenen Defizite im
öffentlichen Haushalt sollen jedoch in folgenden Aufschwungphasen wieder abgebaut werden.
