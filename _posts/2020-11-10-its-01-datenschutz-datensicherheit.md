---
layout: post
title: "01. Datenschutz & Datensicherheit"
date: 2020-11-10 08:30:00 +0200
category: 3. Lehrjahr
tags: [IT-Systeme]
---

## Allgemeines

Datenschutz heißt, dass man Selbstbestimmt entscheiden kann

- wem man,
- wann und
- zu welchem Zweck seine Daten zur Verfügung stellt.

Datensicherheit bezeichnet den Schutz der Daten mithilfe von

- Vertraulichkeit,
- Integrität und
- Verfügbarkeit.

Sind Datenschutz & -sicherheit gewährleistet, so sind die deutschen rechtlichen Grundlagen auf Datenschutz
gewährleistet.

## Risiken und Schutzmaßnahmen von und für Daten

| RISIKO / GEFAHR                 					| SCHUTZ DURCH                                                  |
| ------------------------------------------------- | ------------------------------------------------------------- |
| Physikalisch (Einbruch)         					| Kensington™©®-Schlösser, Videoüberwachung                     |
| Softwarelücken            					    | Betriebsystemupdates/Upgrades/Trennen vom Netzwerk            |
| Hackangriffe                    					| VPN, Firewall, IDS/IPS                                        |
| Phishing                        					| Schulen der Mitarbeiter + Content Filtering + Proxy           |
| Hardwaredefekte               			        | Backup auf ein NAS/Cloud/SAS/..., Reservebausteine bereithalten, RAID-Systeme (RAID 1, 5, 10) |
| Serverausfall                                     | Redundante Infrastruktur (HA, Cluster, ...)                   |
| Diebstahl                                         | Festplattenverschlüsselung / Datenverschlüsselung             |
| "Fremde Mächte" fragen Daten an                   | Datensparsamkeit + Verschlüsselung                            |
| Hackangriffe                    					| Systeme von allen Netzwerken trennen (airgap)                 |
| Inkompatibilitäten mit neuer Software             | Daten zeitnah migrieren                                       | 
| Unautorisierte Zugänge innerhalb des Betriebes    | Mitarbeiter mit entprechenden Rechten versehen                |
| Viren-/Schadsoftware                              | Update der Betriebssysteme / aktuelle Antivirensoftware       |
| Bruteforce-Angriffe                               | Cloudflare™©®/Fail2Ban                                        |
| Festplattenausfälle/-beschädigungen               | Regelmäßige Backups der Nutzerrechner/Server                  |
| Windoof® screensaver zeigt Kundenbilder           | Secreensaver deaktivieren                                     |
| ISP-downtimes                                     | hybride, (Kabel + DSL + LTE/5G) redundante Uplinks            |
| Außendienst hat auswärts Probleme                 | Remote Wartungs Software --> TeamViewer/VNC/...               |
| Fragwürdige Downloadangebote                      | Adblocker                                                     |
| Nutzer installieren fake Browser addons           | Rechteverwaltung                                              | 
| Benutzer löschen wichtige Daten                   | Rechteverwaltung, Backups, VCS                                |
| Hackerangriffe v2                                 | Cyberwehr + Hackback + Revengehacker                          |
| Admins wollen ihre Systeme nicht sichern          | dunno, siehe mebis™                                           |
| Admin gefeuert -> Festplatten wipe                | neue Firma starten                                            | 

