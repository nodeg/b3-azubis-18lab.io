---
layout: post
title:  "14. Wide Area Networking"
date:   2020-05-17 09:30:00 +0100
category: 2. Lehrjahr
tags: [Vernetzte Systeme]
---

Das lokale Netzwerk des AH-Nettmann wurde von Ihnen bereits auf den neuesten Stand gebracht. Mit dem nun anstehenden
Thema betrachten wir den Zugang zum Weitverkehrsnetz (WAN). Aktuelle Entwicklungen fordern die Anpassung der
Internetzugangstechnik (Stichwort: All-IP). Nahezu jede Firma und jeder Haushalt hat inzwischen einen Internetanschluss.
Klassische WAN-Technologien wie z.B. X.25 oder Frame-Relay sind nicht für den Internetzugang konzipiert und führen
inzwischen ein Spartendasein.

![VNS - WAN - 1](/assets/img/VNS/14-wan-technologien.png)

Auch LAN-Technologien sind für einen Internetzugang bedingt geeignet. Ethernet und WLAN wurden für den Einsatz in
lokalen Netzen entwickelt und finden dort ihren Einsatz. Eine Weitverkehrstechnik hat spezielle Anforderungen zu
erfüllen und bietet auf den OSI-Schichten 1 und 2 eigene Lösungen (Technik, Protokolle etc.).

Mit dieser Aufgabenstellung sollen Kriterien für die Auswahl einer geeigneten Internet-Zugangstechnik für das
AH-Nettmann definiert und nach Feststellung der Anforderungen, der Geschäftsleitung ein geeignetes Produkt empfohlen
werden.

## DSL

Mit den Informationen der Geschäftleitung entwerfen Sie einen Plan zur Umsetzung. Die Auswahl der richtigen
Zugangstechnik ist von entscheidender Bedeutung. Internet-Sevice-Provider bieten verschiedene DSL-Varianten für Privat-
und Geschäftskunden.

![VNS - DSL - 1](/assets/img/VNS/14-dsl.png)

Drei sehr verbreitete Varianten sind SDSL, ADSL 2 und VDSL. Definieren Sie Unterscheidungskriterien und vergleichen Sie
die DSL-Techniken an diesen Merkmalen!
