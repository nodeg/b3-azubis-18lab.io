---
layout: post
title: "05. ER-Modell: Konzeptionelle Phase"
date: 2019-10-18 10:15:00 +0200
category: 2. Lehrjahr
tags: [Anwendungsprogrammierung]
---

Hier soll in der konzeptionellen Phase das Entlty-Relatlonship-Modell (ER-Modell) verwendet werden, weil es in der
Praxis weit verbreitet Ist und sich für die Entwicklung relationaler Datenbankmodelle bewährt hat. Nachfolgend sind die
Elemente von Datenmodellen beschrieben.

Eine Entität (engl. entity): Ist eine eindeutige identifizierbare Einheit (z.B. Individuum (Schüler), Objekt (Raum),
abstraktes Objekt (Kurs), Ereignis (mdl. Prüfung))

Eine Entitätsmenge (engl. entity set): Fasst alle Entitäten zusammen, die durch gleiche Merkmale nicht notwendigerweise
aber durch gleiche Merkmalsausprägungen, charakterisiert sind. (z.B. alle Schüler)

Zur grafischen Darstellung von Entitätsmengen werden Rechtecke verwendet.

Eine Beziehung: Ist die Wechselwirkung zwischen zwei Entitäten (Entitätsmengen) und wird ausgedrückt durch den
Beziehungstyp (Kardinalität der Assoziation)

Zur grafischen Darstellung von Beziehungen werden Rauten verwendet.

| Anzahl der Entität die einer anderen Entität zugeordnet werden können | Symbol     | Bezeichnung des Kardinalitätstyps der Assoziation |
|-----------------------------------------------------------------------|------------|---------------------------------------------------|
| Keine oder maximal eine                                               | 1          | einfach                                           |
| Keine, eine oder mehrere                                              | n (oder m) | mehrfach (viele, komplex)                         |

![AWP - ER-Beziehungen](/assets/img/AWP/05-ER-Beziehungen.png)

Ein Attribut: Beschreibt eine bestimmte Eigenschaft, die sämtliche Entitäten einer Entitätsmenge aufweisen (z.B. Name).

Das Entity-Relationship-Modell (semantisches Modell)

Zwischen 2 Entitäten kann es mehrere Beziehungen geben. Diese müssen aber etwas unterschiedliches beschreiben.

Bei der Bestimmung der Kardinalitäten bietet sich folgender Fragesatz an. Dabei erfolgt die Überprüfung stets in beiden
Richtungen. Die Kardinalität wird dabei stets an der gegenüberliegenden Entität festgehalten.

Fragesatz: Eine konkrete <Entität> hat <Beziehung> zu wie vielen <Entitäten>?