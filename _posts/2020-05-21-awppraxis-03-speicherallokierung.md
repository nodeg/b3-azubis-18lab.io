---
layout: post
title: "03. Speicherallokierung"
date: 2019-10-22 10:00:00 +0200
category: 2. Lehrjahr
tags: [Anwendungsprogrammierung, Praxis]
---

## Dynamisches Allokieren von Speicher

Es gibt in C++ die Möglichkeit, ohne das Vorhandensein einer "normalen" Variable einem Zeiger Speicher zuzuweisen.

````c
float *pfZeiger;
pfZeiger = new float;
````

Die erste Zeile des Beispiels erstellt einen Zeiger für eine Variable des Typs float und die zweite Zeile allokiert
Speicher und weißt die Anfangsadresse dieses Speichers an `pfZeiger` zu.

Der `new`-Operator fordert Speicher vom Betriebssystem an. Das Betriebssystem liefert daraufhin als Ergebnis eine
Speicheradress, die die Anfangsadresse des allokierten Speicherbereiches darstellt. Hierbei sind zwei Dinge zu beachten:

1. Der Speicher der durch `new` reserviert wurde, hat keinen Namen.
2. Der Speicher der reserviert wurde hat irgendeinen Wert, da die Variable nicht initialisiert wurde.

Ansonsten ist alles wie gehabt. Der Zeiger zeigt auf eine andere Variable und hat daher als Wert die Speicheradresse
jener (unbenannten) Variable.

Der Zugriff auf die rechts stehende Variable kann jetzt nur noch durch den Zeiger passieren, weil ja kein Variablenname
vorhanden ist! Ansonsten ist die Variable wie ein Zeiger zu behandeln.

Lesend: `cout << *pfZeiger;`  
Schreibend: `*pfZeiger = 3.1315926` 
