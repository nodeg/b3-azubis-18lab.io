---
layout: post
title:  "09. Routing"
date:   2019-12-18 10:00:00 +0100
category: 2. Lehrjahr
tags: [Vernetzte Systeme]
---

## Fahrplan der Netze

### Routingtabellen weisen den Weg

Routing ist die Wegfindung für ein Datenpaket in einem Netzwerk. Dabei wird der Weg zum Ziel anhand einem oder
mehreren Kriterien (metric) ermittelt. Je mehr Kriterien berücksichtigt werden müssen, desto genauer und gezielter ist
der Weg zum Ziel. Aber desto (zeit-)aufwendiger ist die Bestimmung oder Berechnung des Wegs. Das maßgebliche Hilfs-
mittel beim Routing ist die Routing-Tabelle.

### Vielseitig und flexibel

Der Anschluss eines Heim- oder Firmennetzwerks an das Internet erfolgt über Access-Router. Über die weit verbreitete
Internetzugangstechnik DSL oder auch eine Mobilfunktechnik verbindet der Router das lokale Netzwerk (LAN) per
"Default-Route" mit dem WAN. Das verbinden lokaler Netzwerke steht hier nicht im Vordergrund, ist aber häufig möglich.

Abhängig vom speziellen Gerät bieten diese Router häufig ein eingebautes Modem und eine Vielzahl weiterer Funktionen.
Sie werden deshalb auch 1ntegrated-Serviouter (ISR) genannt Firewall und VPN-Gateway, Telefonanlage, Switch oder
WLAN-Access-Point sind Erweiterungen die über die eigentliche Routing-Funktion hinausgehen.

Zwischen diesen beiden Routertypen gibt es eine Vielzahl von Geräten, die abhängig von ihrem Einsatzbereich diverse
Anforderungen mit verschiedenen Leistungsmerkmalen erfüllen. Das spiegelt sich zum Beispiel in deren Leistungsaufnahme
(15- 5000 W) oder im Preis wieder.

### Routing-Tabelle

Die Routing-Tabelle enthält eine umfassende und aktuelle Wegbeschreibung durch das Netz. In ihr sind alle bekannten
Routen eingetragen. Die Routing-Tabelle wird entweder manuell gefüllt, also statische Routen angelegt, oder dynamisch im
Austausch mit anderen nahegelegenen Routern gepflegt. Änderungen der möglichen Routen müssen beim statischen Routing
händisch vom Administrator gepflegt werden. Beim dynamischen Routing werden die Routing-Tabellen von den Routern
selbständig gepflegt und an die Netzstruktur angepasst. Z. B. auch beim Ausfall von Routern oder Übertragungsstrecken.
Die Routing-Tabelle enthält möglicherweise folgende Angaben:

- alle bekannten Netzwerkadressen
- Verbindungsarten in andere Netzwerke
- Weginformationen zu anderen Routern
- Verbindungskosten

### Routenwahlmethoden

Der Aufbau einer Routing-Tabelle entscheidet, welche Routenwahlmethode verwendet wird. Diese Methode ist ein
Algorithmus, der die Einträge in der Routing-Tabelle benutzt um die Route zu berechnen. Die häufigsten
Routenwahlmethoden sind der Distance-Vector-Algorithmus (DVA) und der Link-Status-Algorithmus (LSA).

#### LSA - Link-Status-Algorithmus

Der LSA bestimmt die Route anhand des Status der Verbindungen, also deren Verfügbarkeit und Geschwindigkeit. Ein
spezieller Sortleralgorithmus ermittelt dann z. B. den kürzesten Weg (Shortest Path) zum Ziel. Beim Link-Status-Routing
(LSR) werden die Änderungen In der Routing-Tabelle per Multlcast zwischen den Routern ausgetauscht. In der
Routing-Tabelle ist deshalb die gesamte Netzstruktur abgebildet. Der Router kennt deshalb jede erdenkliche Route.
Protokolle nach LSA werden als externe oder exterior Routing-Protokolle bezeichnet, die netzübergreifend genutzt werden.

#### OVA - Distance-Vector-Algorithmus

Jede Route wird anhand einiger Kriterien klassifiziert. Aus allen Routen wird dann die mit den optimalen Voraussetzungen
gewählt. Besonders bei weit entfernten Zielen mit vielen Routen, lässt.sich so die optimale Route ermitteln. Beim
Distance-Vector-Routing (DVR) werden die Routing-Tabellen m it dem direkten Nachbar-Router ausgetauscht. Die
Routing-Tabelle wird in periodischen Abständen ausgetauscht. Das führt zu zusätzlichem Datenverkehr zwischen den
Routern. DVR-Protokolle werden als interne oder interior Routing Protokolle bezeichnet, die in lokalen Netzen genutzt
werden.

#### RIP - Routing Information Protocol

Das RIP ist ein Distance-Vector-Algorithmus, also ein Distance-Vector-Routing-Protokoll. Es ist das einfachste und meist
genutzte Routing-Protokoll. Die Fähigkeiten moderner Netze werden von RIP allerdings nicht berücksichtigt. Im
einfachsten Fall speichert RIP in seiner Routing-Tabelle neben Netzwerkadresse und abgehende Schnittstelle nur die
Anzahl der Stationen (Hops) die ein Datenpaket bis zum Zielnetz überwinden muss. Ein Hop-Eintrag von 16 gilt als
unendlich und bedeutet, dass dieses Netz nicht erreichbar ist. Deshalb ist RIP in Netzwerken mit mehr als 15
Zwischenstationen nicht geeignet. Es wird daher auch nur in lokalen Netzwerken eingesetzt, wo die Netzübergänge (Router)
von gleicher Qualität sind und die Netzwerkstruktur nur selten verändert wird.

Die Routing-Tabellen werden von den Routern alle 30 Sekunden mit dem benachbarten Router ausgetauscht. Dies führt zu
einem erhöhten Datenverkehr zwischen den Routern. Fällt ein Router aus, kann es mehrere Minuten dauern, bis diese
Information und die entsprechend geänderte Routing-Tabelle übermittelt wurden.

## Routing - Wegewahl durch die Netze

> von Wolfgang Schulte - 2004

Routing ist eine Funktion zur Wegebestimmung von Paketübertragungen in Netzwerken, implementiert in den Routern. Den
besten Weg durch die Vielzahl der Netze zu finden, ist nicht immer einfach. Verschiedene Routing-Protokolle stehen zur
Auswahl.

Folgende Definition findet sich im RFC 1983 Internet Users Glossary. Routing ist der Prozess der Auswahl der richtigen
Schnittstelle und das nächsten Hops (Router) für (Daten-)Pakete, die weitergeleitet werden sollen.  
Ein Router muss die folgenden drei Bedingungen erfüllen, um Pakete weiterzuleiten:

- Die entsprechenden Routing-Protokolle müssen im Router aktiv sein.
- Das Zielnetz muss bekannt sein oder eine Alternative, die zum Zielnetz führen könnte.
- Der Router muss seine aktive Schnittstelle in Richtung dees Zieles auswählen.

Im englischen Sprachgebrauch wird zwischen Routed-Protokollen und Routing-Protokollen unterschieden. Mit
Routed-Protokollen und Routing-Protokollen unterschieden. Mit Routed-Protokoll werden alle Protokolle bezeichnet, die
durch die Router weitergeleitet werden, um Benutzterinformation vom Sender zum Empfänger weiter zu leiten. Ein Router
muss in der Lage sein, die Informationen, die durch das Routed Protokoll gegeben sind, zu interpretieren. Beispiele der
Routed-Protokolle sind unter anderem das Internetprotokoll (IP) und von Novell das Internetwork Packet Exchange (IPX)
Protocol.

Mit Routing-Protokollen werden die protokolle bezeichnet, welche die Wegewahl durch spezielle Routing-Algorithmen
ermöglichen. Es werden nur Tabllen mit Informationen, die die Weitergabe der Benutzerinformationen unterstützen, an
andere Router weitergeleitet, keine Benutzerinformationen.

Das Austauschen der Routing-Tabellen darf die Netzbelastung nicht übermässig beanspruchen. Die Routing-Tabllen sollen
nicht zu groß werden und überschaubar bleiben. 

Beispiele für öffentliche und private Routing-Protokolle sind das Routing Information Protocol (RIP), von Cisco das
Interior Gateway Routing Protocol (IGPR), das Open Shortes Path First (OSPF) oder das Border-Gateway-Protokoll (BGP).

Eine andere Unterscheidung der Routing-Protokolle ist mit ihrem Einsatzgebieten gegeben, zum Beispiel, ob sie innerhalb
der eignen Netzwerke oder zwischen den  einzelnen Netzen wirken.

### Interior Gateway Protokoll (IGP)

Ein Protokoll, das seine Routing-Informationen zu Routern innerhalb eines autonomen Systems verteilt, wird mit IGP
bezeichnet. Der Begriff "Gateway" ist dabei historisch zu sehen und ist heute gleichzusetzen mit Router. Beispiele für
IGP sind RIP für kleinere Netze, IGRP als propritäres Protokoll von Cisco und für größere Netze OSPF.

Protokolle, die zwischen autonomen Systemen wirken, werden als EGP (Exterior-Gateway-Protokoll) bezeichnet. Es gitb auch
noch ein spezielles Protokoll, in RFC 904 beschrieben, das ebenfalls Exterior Gateway Protocol (EGP) genannt wird, aber
durch das BGP weitgehend abgelöst wurde. Das beaknnteste EGP ist das Border-Gateway-Protokoll in der Version 4.

### Autonomes System (AS)

Mit AS wird eine Gruppe von Netzen unter einer gemeinsamen Administration mit, in der Regel einheitlichem IGP,
bezeichnet. Ein solches AS kann in mehrere Bereiche, Areas genannt, unterteilt werden. Ein autonomes System erhält eine
16 (in Zukunft 32) Bit große Identifikation, die durch die Internet Assigned Numbers Authority (IANA) vergeben wird.

Eine andere Klassifizierung der Routing-Protokolle wird durch die benutzten Routing-Algorithmen vorgenommen.

### Distance-Vector-Routing-Protokolle

Mit diesem Algortihmus wird die Anzahl der Hops (Sprünge) in einem Pfad bestimmt, um den kürzesten Weg vom Sender zum
Ziel zu finden. Der Distance-Vector-Alorithmus erwartet von jedem Router die Übertragung seiner vollständigen
Routing-Tabelle bei jedem Update zum Nachbar-Router. Mit diesem Algorithmus können auch Schleifen in der Routing-Tabelle
gebildet werden. Der benötigte Rechenaufwand im Router ist jedoch geringer als zum Beispiel bei Link-State-Protokollen.
Der Distance-Vektor-Routing-Algorithmus wird auch nach seinen Entwicklern Bellmann-Ford-Routing-Algorithmus genannt. Das
Routing-Protokoll RIP nutzt dieses Verfahren.

### Link-State-Routing-Protokolle

Dabei handelt es sich um einen Routing-Algorithmus, bei der die Router ihre Routing-Information (zum Beispiel Kosten
oder die Last) zum Erreichen seiner Nachbarn an alle Router im Netz weitersenden.

Der Link-State-Algorithmus schafft eine einheitliche Information des Netzes in einer Baumstruktur und ist deshalb nicht
anfällig für Routing-Schleifen.

Für die Berechnung der Routing-Informationen wird im Router eine höhere Rechnerleistung gefordert. Der
Shortest-Path-First-Algorithmus oder auch nach seinem Entwickler Dijkstra-Algorithmus genannt, bildet mit seiner
Routing-Tabelle eine Baumstruktur. Das Protokoll Open Shortest Path First (OSPF) gehört in diese Klasse von
Routing-Protokollen.

### Statische Route

Wege durchs Netz, die explizit konfiguriert und in der Routing-Tabelle einzutragen sind, werden als statische Route
bezeichnet. Diese haben Vorrang vor einer dynamischen wegewahl per Routing-Protokoll. Das bietet sich insbesondere bei
so genannten End-Netzen (Stub Networks) an, um den Overhead im Netzwerk zu vermeiden. An Stelle der Aufrechterhaltung
von speziellen Netztopologiekenntnissen in den Routern kann durch eine so genannte Default Route der Weg zu einem Netz
vorgegeben werden.

### Daynamisches Routing

Ein Routing das sich automatisch an Änderungen in der Netztopologie oder an belastungslimits orientiert wird dynamisches
Routing oder adaptives Routing genannt. Die Router lernen den Pfad, zum Ziel durch regelmäßige Updates von anderen
Routern kennen.

Drei Mechanismen sind bei den Routing-Protokollen eingeführt worden, um eine gewisse Stabilität im Austausch der
Routing-Informationen im Netz zu errreichen.

- Split Horizons: Es werden keine Update-Nachrichten in die Rihctung weiter gesendet, aus der sie selbst empfangen
  werden. Dies verhindert Routing-Schleifen mit direkt ebanchbarten routern.
- Hold-Down-Timer: werden eingesetzt, um reguläre Update-Nachrichten davon abzuhalten, eine Route wieder einzusetzen,
  die unbrauchbar geeworden sein könnte. Nach dem Empfang einer Update-Nachricht läuft der Timer ab, um in dieser Zeit
  keine weitere zum Beispiel reguläre zeitliche Update-Information zu akzeptieren.
- Poison-Reverse-Updates: Wählrend Split Horizons Schleifen zwischen benachbarten Routern vermeiden, werden mit
  Poison-Reverse-Updates größere Schleifen verhindert. Routing-Updates, die über eine bestimmte Schnittstelle gesendet
  werden, kennzeichnen jedes über diese Schnittstelle erlernte Netz als nicht erreichbar. Das Anwachsen von
  Routing-Metriken ist ein Anzeichnen von Routing-Schleifen. Cisco sendet Poison-Reverse-Updates, wenn eine
  Routing-Metrik um mehr als den Faktor 1,1 anwächst.

### Routing-Metrik

Mit Hilfe der Routing-Metrik wird vom Routing-Algorithmus entschieden ob ein Weg durch das Netzwerk besser ist als ein
anderer Weg. Diese Information wird in der Routing-Tabelle gespeichert. Metriken sind unter anderem Bandbreite, Kosten
des Pfades, Verzögerung, Sprungzähler (Hop Count), Last und andere. Nicht alle Routing-Protokolle nutzen alle Parameter.

Ein Maß der Vertrauenswürdigkeit für die Auswahl eines bestimmten Weges durch das Netz ist die administrative Distanz.
In Cisco-Routern wird diese in einem Wert von 0 bis 255 angegeben. Je höher der Wert, desto geringer die
Vertrauenswürdigkeit. Der Netzwerk-Administrator kann die Standardwerte überschreiben.

Beispiele für die administrative Distanz in Cisco Routern: Static Route = 0, EIGRP = 90, IGRP = 100, OSPF = 110,
RIP = 120.

Die Werte besagen, dass die statische Route, die der Netzwerk-Adminstrator selber eingegeben hat, die höchste
Vertrauenswürdigkeit besitzt. Mit dem Wert 120 für RIP wird dabei dei geringste Vertrauenswürdigkeit angenommen.
