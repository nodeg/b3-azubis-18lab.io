---
layout: post
title:  "04. Kontrollstrukturen"
date:   2019-04-04 06:00:00 +0100
category: 1. Lehrjahr
tags: [Anwendungsprogrammierung]
---

## Eindimensionale Felder

Eindimensionale Felder werden auch Arrays genannt. Sie befähigen uns mehrere Werte vom gleichen Datentyp gruppiert anzulegen und zu verwalten. Dies erspart uns Konstrukte wie bspw. `zahl_1, zahl_2, zahl_3, ..., zahl_n`.

Formal wird ein Array folgendermaßen definiert: `Datentyp Name[Größe];`. Arrays werden im Arbeitsspeicher immer zusammenhängend reserviert.

Wenn wir einen Array vom Datentyp `int` mit der Länge 10 definieren wollen, ergibt das: `int zahlen[10];`. Das Problem ist, dass an der Stelle im Arbeitsspeicher bereits Werte standen. Das Array ist also nicht leer. 

Für das Problem, dass Arrays nach der Definition nicht leer sind gibt es zwei Lösungsmöglichkeiten:
1. Manuelles Setzen der Werte mit einer `for`-Schleife
2. Bei der Initialisierung sofort alle Werte auf 0 setzen: `int zahlen[10] = { 0 };`

Weitere Beispiele um Arrays zu definieren:
````c++
int zahlen_1[10] = { 2 };           // 2 0 0 0 0 0 0 0 0 0
int zahlen_2[10] = { 1, 2, 3 }      // 1 2 3 0 0 0 0 0 0 0
````

Um auf einen Array lesend zuzugreifen ist folgende Syntax zu verwenden: `variable = zahlen[Platz];`. Hiermit würde in der Variable `variable` der Wert aus `zahlen` an Platzierung `Platz` gespeichert werden.  
Um auf einen Array schreibend zuzugreifen ist folgende Syntax zu verwenden: `zahlen[Platz] = 5;`. Hiermit wird dem Array `zahlen` an Platz `Platz` der Wert `5` zugewiesen.

**Bei lesendem um schreibendem Zugriff ist zu beachten, dass C++ Nullbasiert arbeitet!** Nullbasiert bedeutet, dass der erste Platz im Array nicht mit `1` nummeriert ist, sondern mit `0`. Die zweite Position in einem Array wird also mit `array[1]` gelesen oder gesetzt! 

## Einfache Sortierverfahren

### Bubblesort

### Quicksort

## Zweidimensionale Arrays
