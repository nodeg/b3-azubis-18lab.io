---
layout: post
title:  "11. Firma"
date:   2019-02-15 06:35:00 +0100
category: 1. Lehrjahr
tags: [Betriebswirtschaftliche Prozesse]
---

# Firma

## Im handelsrechtlichen Sinne

````
§ 17 HGB [Begriff]
(1) Die Firma eines Kaufmanns ist der Name, unter dem er seine Geschäfte betreibt und die Unterschrift abgibt.
(2) Ein Kaufmann kann unter seiner Firma klagen und verklagt
````

Die Firma ist nicht das Gebäude oder der Ort, wie der umgangssprachliche Einsatz des Begriffes es häufig vermuten lässt (Beispiel: „Ich muss später noch in die Firma“). Die Firma ist der im Handelsregister eingetragene
Name, unter dem ein Kaufmann sein Handelsgewerbe betreibt und nach außen auftritt (firmiert).
Der Kaufmann kann unter seiner Firma klagen und verklagt werden.
Das Recht an einer bestimmten Firma ist gesetzlich geschützt. Das Gesetz schützt den Inhaber einer
Firma beispielsweise vor der Annahme einer nicht deutlich abweichenden Firma durch einen anderen Kaufmann am selben Ort [§ 30 HGB]. Bei unrechtmäßiger Firmenführung durch ein anderes Unternehmen kann der Geschädigte die Unterlassung des Gebrauchs der Firma und unter bestimmten Voraussetzungen auch Schadensersatz verlangen [§ 37 II HGB].

````
§ 18 [1][Firma des Kaufmanns]
(1) Der Firma muss zur Kennzeichnung des Kaufmanns geeignet sein und Unterscheidungskraft besitzen.
(2) 1 Die Firma darf keine Angaben enthalten, die geeignet sind, über geschäftliche Verhältnisse, die für die angesprochenen Verkehrskreise wesentlich sind, irrezuführen. 2 Im Verfahren vor dem Registergericht wird die Eignung der Irreführung nur berücksichtigt, wenn sie ersichtlich ist.
````

Unabhängig von der Rechtsform des Unternehmens muss eine Firma die folgenden Funktionen erfüllen:
1. Sie muss sich deutlich von anderen Firmen unterscheiden [§ 18 I HGB]
  (Unterscheidungskraft).
2. Die Geschäftsverhältnisse müssen ersichtlich sein [§ 19 I HGB].
3. Die Haftungsverhältnisse müssen offen gelegt werden [§ 19 II HGB].
4. Die Firma darf nicht irreführend sein (Irreführungsverbot nach 5 18 II HGB) d. h. sie darf
  keine Angaben enthalten, die ersichtlich geeignet sind, über geschäftliche Verhältnisse, die
  für die angesprochenen Verkehrskreise wesentlich sind, irrezuführen.

Nur eine Firma die allen vier Funktionen Rechnung trägt, ist grundsätzlich eintragungsfähig ins
Handelsregister.

## Bestandteile des Firmennamens

Eine Firma besteht aus dem Firmenkern und einem oder mehreren Firmenzusätzen. Zwingend vorgeschrieben
sind die Rechtsformzusätze wie z. B. eingetragene(r) Kauffrau/-mann (e.Kfr., e.K.) oder
bspw. Aktiengesellschaft (AG). Freiwillige Firmenzusätze stärken den Informationsgehalt einer
Firma z. B. Hans Kern e.K., Weinhandlung.

## Arten der Firma

Grundsätzlich lässt das Handelsrecht den Kaufleuten sehr viele Freiräume bei der Wahl Ihrer Firma. Sofern die Bezeichnung sich deutlich von anderen Firmen unterscheidet und nicht in die Irre führt, kann jeder Name gewählt werden. Durch entsprechende Namenszusätze müssen aber die Geschäfts- und Haftungsverhältnisse deutlich gemacht werden. Die unterschiedlichen Firmen lassen sich in folgenden Arten zusammenfassen:  
$$\rightarrow$$ **Personenfirmen** enthalten einen oder mehrere Personennamen (z. B. Carola Müller OHG, Schneider & Bauer KG).  
$$\rightarrow$$ **Fantasiefirmen** sind erdachte Namen (z. B. Fantasia Verlagsgesellschaft mbH, Impex OHG, Infineon AG).  
$$\rightarrow$$ **Sachfirmen** sind dem Zweck (dem Gegenstand) des Unternehmens entnommen (z. B. Vereinigte Lebensmittelfabriken Köln GmbH, Hamburger Metallwarenfabrik AG).’  
$$\rightarrow$$ **Gemischte Firmen** enthalten sowohl einen oder mehrere Personennamen, einen dem Gegenstand (Zweck) des Unternehmens entnommenen Begriff und/oder einen Fantasienamen (z. B. Dyckerhoff Zementwerke Aktiengesellschaft; Arzneimittelgroßhandlung Peter & Schmid OHG; Fantasia Ferienpark GmbH). Gemischte Firmen kommen sowohl bei Einzelunternehmen, Personengesellschaften und Kapitalgesellschaften vor.

````
§ 19 [1][Bezeichnung der Firma bei Einzelkaufleuten, einer OHG oder KG]
(1) Die Firma muss, auch wenn sie nach den §§ 21 , 22 , 24 oder nach anderen gesetzlichen Vorschriften fortgeführt wird, enthalten:
    1. bei Einzelkaufleuten die Bezeichnung „eingetragener Kaufmann“, „eingetragene Kauffrau“ oder eine allgemein verständliche Abkürzung dieser Bezeichnung, insbesondere „e.K.“, „e.Kfm.“ oder „e.Kfr.“;
    2. bei einer offenen Handelsgesellschaft die Bezeichnung „offene Handelsgesellschaft“ oder eine allgemein verständliche Abkürzung dieser Bezeichnung;
    3. bei einer Kommanditgesellschaft die Bezeichnung „Kommanditgesellschaft“ oder eine allgemein verständliche Abkürzung dieser Bezeichnung. 
(2) Wenn in einer offenen Handelsgesellschaft oder Kommanditgesellschaft keine natürliche Person persönlich haftet, muss die Firma, auch wenn sie nach den §§ 21 , 22 , 24 oder nach anderen gesetzlichen Vorschriften fortgeführt wird, eine Bezeichnung enthalten, welche die Haftungsbeschränkung kennzeichnet
````

## Firmengrundsätze

### Firmenwahrheit und -klarheit.

Die Firma darf nicht über Art und/oder Umfang des Geschäfts täuschen (§18 II HGB).

### Firmenausschließlichkeit

Jede neue Firma muss sich von anderen an demselben Ort oder in derselben Gemeinde bereits bestehenden und in das Handels- oder Genossenschaftsregister eingetragenen Firmen deutlich unterscheiden. Bei gleichen Familiennamen der Inhaber muss ein Firmenzusatz eine eindeutige Unterscheidung ermöglichen (siehe § 30 I HGB).

### Firmeneinheitlichkeit

Ein Kaufmann darf für ein Unternehmen nur eine Firma führen. Die Eindeutigkeit der Firmenbezeichnung muss gewahrt werden, damit eine zuverlässige Identifikation der Firma möglich ist.

### Firmenbeständigkeit

Die bisherige Firma kann beibehalten werden, wenn sich der Name des Inhabers ändert (z.B. bei Heirat), das Unternehmen durch einen neuen Inhaber fortgeführt wird (z.B. bei Verkauf oder Erbschaft) oder bei Eintritt eines zusätzlichen Mitinhabers (Gesellschafters) (vgl. § 21 – 24 HGB). Auch wenn die Firma fortgeführt wird, muss sie die zwingend vorgeschriebenen Rechtsformzusätze wie „eingetragener Kaufmann“,„eingetragene Kauffrau“, „offene Handelsgesellschaft“ oder „Kommanditgesellschaft“ bzw. die allgemein verständlichen Abkürzungen dieser Bezeichnungen enthalten [§ 19 I HGB].

### Firmenöffentlichkeit

Jeder Kaufmann ist verpflichtet, die Firma und den Ort seiner Handelsniederlassung sowie deren
spätere Änderungen zur Eintragung in das Handelsregister anzumelden [§§ 29, 31 HGB]. Damit
wird erreicht, dass die Öffentlichkeit (also Kunden, Lieferanten, Banken, Behörden) z.B. erfährt,
unter welcher Firma Geschäftsvorgänge abgewickelt werden.

## Pflichtangaben auf Geschäftsbriefen

Für sämtliche kaufmännischen Unternehmen’ sind folgende Angaben auf Geschäftsbriefen, die an einen bestimmten Empfänger gerichtet sind, verpflichtend vorgeschrieben

````
(1) Auf allen Geschäftsbriefen des Kaufmanns, die an einen bestimmten Empfänger gerichtet werden, müssen seine Firma, die Bezeichnung nach § 19 Abs. 1 Nr. 1, der Ort seiner Handelsniederlassung, das Registergericht und die Nummer, unter der die Firma in das Handelsregister eingetragen ist, angegeben werden.
§ 37a [1][Angaben auf Geschäftsbriefen]
(2) Der Angaben nach Absatz 1 bedarf es nicht bei Mitteilungen oder Berichten, die im Rahmen einer bestehenden Geschäftsverbindung ergehen und für die üblicherweise Vordrucke verwendet werden, in denen lediglich die im Einzelfall erforderlichen besonderen Angaben eingefügt zu werden brauchen.
(3) 1Bestellscheine gelten als Geschäftsbriefe im Sinne des Absatzes 1. 2Absatz 2 ist auf sie nicht anzuwenden.
(4) 1Wer seiner Pflicht nach Absatz 1 nicht nachkommt, ist hierzu von dem Registergericht durch Festsetzung von Zwangsgeld anzuhalten. 2§ 14 Satz 2 gilt entsprechend.
````
