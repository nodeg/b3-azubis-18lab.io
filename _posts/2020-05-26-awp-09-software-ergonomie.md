---
layout: post
title: "09. Software Ergonomie"
date: 2020-05-26 10:49:17 +0200
category: 2. Lehrjahr
tags: [Anwendungsprogrammierung]
---

Ein Dialog stellt die Interaktion zwischen einem Benutzer und einem interaktiven System dar. Der Benutzer setzt im
Rahmen der Interaktion eine Folge von Eingaben an das System ab und das System beantwortet die Eingaben durch
entsprechende Ausgaben.

Zur Realisierung der Software-Ergonomie (= Optimierung einer Software an die Bedürfnisse von Benutzern im Hinblick auf
die Einhaltung wissenschaftlich erforschter Regeln) sind bei der Entwicklung von Software die 7 Grundsätze der
Dialoggestaltung nach DIN EN ISO 9241-110 zu beachten. Durch die Berücksichtigung der Grundsätze sollen die folgenden
Qualitätsziele erreicht werden:

| Vermeidung...                                        | Erhöhung...                                                                                           |
|------------------------------------------------------|-------------------------------------------------------------------------------------------------------|
| ... unnötiger Arbeitsschritte.                       | ... der Produktivität durch effiziente Gestaltung von Arbeitsprozessen.                               |
| ... irreführender oder unzureichender Informationen. | ... der Benutzbarkeit des Systems (Usability) im Hinblick auf den Einsatz im Bereich des Kunden.      |
| ... unerwarteter Ausgaben des Systems.               | ... des Gesamterlebnisses durch Optimierung der Eindrücke des Benutzers vom System (User Experience). |
| ... mangelhafter Fehlerbehebung.                     |                                                                                                       |

Die 7 Grundsätze der Dialoggestaltung sollen in allen Phasen der Projektdurchführung berücksichtigt werden. Aus diesem
Grund werden die Grundsätze im Folgenden kurz beschrieben:

1. Aufgabenangemessenheit: Der  Benutzer erhält durch das interaktive System Unterstützung bei der Erledigung seiner
   Aufgaben.
2. Selbstbeschreibungsfähigkeit: Dem Benutzer ist zu jeder Zeit klar, wo er sich im Dialog befindet, welche Handlungen
   ausgeführt werden können und wie er die Handlungen ausführen kann.
3. Fehlertoleranz: Der Benutzer kann das Arbeitsziel trotz fehlerhafter Eingaben entweder mit keinem oder mit geringem
   Korrekturaufwand erreichen. Dazu werden Mittel der Fehlerkorrektur, Fehlererkennung und Fehlervermeidung eingesetzt.
4. Lernförderlichkeit: Der Dialog unterstützt den Benutzer beim Erlernen der Nutzung des Systems und leitet den Benutzer
   an.
5. Steuerbarkeit
6. Erwartungskonformität
7. Individualisierbarkeit
