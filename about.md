---
layout: page
title: Über uns
permalink: /about/
---

Diese Seite soll die Anlaufstelle für die Azubis der Berufsschule B3 aus dem Lehrjahr 2018 werden im Bereich IT.
Von Azubis für Azubis.

Macht mit! Ihr könnt Änderungen und weitere Materialien durch merge requests im repository vorschlagen!
<https://gitlab.com/B3-Azubis-18/b3-azubis-18.gitlab.io>
