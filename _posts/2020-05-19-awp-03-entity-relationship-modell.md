---
layout: post
title: "03. Entity Relationship Modell"
date: 2019-10-17 10:00:00 +0200
category: 2. Lehrjahr
tags: [Anwendungsprogrammierung]
---

## Hintergrund

Vor der Erstellung einer Datenbank muss eine Anforderungsanalyse durchgeführt und ein Datenbankentwurf erstellt werden.

Im Rahmen der Anforderungsanalyse wird ermittelt, welche Informationen und Vorgänge in der Datenbank gespeichert werden
müssen. Dieser Prozess wird in enger Zusammenarbeit mit dem Fachbereich und ggf. den (zukünftigen) Kunden durchgeführt.
Aus der Anforderungsanalyse resultiert die Anforderungsspezißkation (auch: Pflichtenheft).

Für die Erstellw1g eines Datenbankentwurfs wird die Anforderungsspezifikation genutzt. Dabei wird auf Gmndlage der
ermittelten Anforderungen zunächst ein konzeptioneller Entwurf und anschließend ein physischer Entwurf der Datenbank
erstellt. Der konzeptionelle Entwurf legt den Fokus auf die Gestaltung der logischen Datenstrukturen. Der physische
Entwurf verfolgt das Ziel der Effizienzsteigerung, ohne die logische Struktur der Daten zu verändern.

Im Rahmen des konzeptionellen Entwurfes wird in der Praxis das Entity-Relationship-Modell (übersetzt:
Gegenstands-Beziehungs-Modell, kurz: ERM, ER-Modell, ER-Diagramm) erzeugt. Das Modell dient der Veranschaulichung und
der Beschreibung der logischen Struktur der zu erstellenden Datenbank. Es wird darüber hinaus auch zu Optimierungs-
und Dokumentationszwecken verwendet.

## Das Entity-Relationship-Modell

Entity-Relationship-Modelle setzen sich zusammen aus Symbolen:

| ERM-Symbol                                                      | Beschreibung                                                                                                                                                                                                                        |
|-----------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ![AWP - 03 - Enitity](/assets/img/AWP/03-entity.png)            | Eine Entität (engl. entity) ist eine Zusammenfassung von Objekten mit gleichen Eigenschaften unter einem Oberbegriff.<br><br>**Beispiel**:<br>_Anforderung_: Max und Otto sind Kunden.<br>_Objekte_: Max, Otto.<br>_Entity_: Kunde. |
| ![AWP - 03 - Relationship](/assets/img/AWP/03-relationship.png) | Beziehungen (engl. relationship) beschreiben Wechselwirkungen und Abhängigkeiten zwischen Entitäten.<br><br>**Beispiel**:<br>_Anforderung_: Der Kunde mietet ein Fahrzeug.<br>_Entities_: Kunde, Fahrzeug<br>_Beziehung_: Mieten    |
| ![AWP - 03 - Attribut](/assets/img/AWP/03-attribut.png)         | Ein Attribut bezeichnet eine Eigenschaft einer Entität oder einer Beziehung.<br><br>**Beispiele**:<br>_Anforderung_: Alle Kunden haben eine Kundennummer.<br>_Entity_: Kunde.<br>_Attribut_: Kundennummer.                          |
| ![AWP - 03 - Verbinder](/assets/img/AWP/03-verbinder.png)       | _Anforderung_: Die Miete ist ein Preis, den ein Kunde für ein geliehenes Fahrzeug zahlen muss.<br>_Beziehung_: Mieten<br>_Attribut_: Preis<br><br>**Verbindungslinien** verknüpfen die ERM-Symbole untereinander.                   |

Beziehungen zwischen zwei Entitäten EI und E2 werden wie folgt unterschieden:

- 1:1-Beziehung: Die Beziehung gilt für jedes Objekt der ersten Entität El mit genau einem Objekt der zweiten Entität
  E2.
- 1:n-Beziehung: Die Beziehung gilt für jedes Objekt der ersten Entität El mit n Objekten der zweiten Entität E2.
- n:1-Beziehung: Die Beziehung gilt für n Objekte der ersten Entität E1 mit genau einem Objekt der zweiten Entität E2.
- n:m-Beziehung: Die Beziehung gilt für n Objekte der ersten Entität EI mit m Objekten der zweiten Entität E2.

Beispiel:

- **Anforderung**: Ein Kunde kann nur ein Fahrzeug zur selben Zeit mieten.
- **Bedeutung**: Wenn Max und Otto (=Objekte) als Kunden Fahrzeuge mieten wollen, dann kann jeder von ihnen nur ein
  Fahrzeug zur selben Zeit mieten.
- **Beziehung**: 1:1-Beziehung.
- **Folge**: In dem Entity-Relationship-Modell wird die Beziehung zwischen den Entitäten Kunden und Fahrzeug als
  1:1-Beziehung kenntlich gemacht!
